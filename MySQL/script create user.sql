-- Создание пользователя для приложения
-- обязательно нужно поменять плагин для хранения пароля
CREATE USER IF NOT EXISTS 'parking_db_user'
  IDENTIFIED WITH mysql_native_password BY 'jkvbhd4i6';

-- установка разрешений на работу с созданной БД
GRANT ALL ON CarParkingDB.* TO parking_db_user;

-- установка разрешений на тестовую БД,
-- автоматически создаваемой Django
GRANT ALL ON test_CarParkingDB.* TO parking_db_user;
