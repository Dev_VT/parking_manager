USE CarParkingDB;

DELIMITER $$

CREATE TRIGGER check_employee BEFORE DELETE ON Employee
FOR EACH ROW
	IF ((SELECT count(*) FROM Employee) > 1) THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'One administrator must remain.';
	END IF $$

DELIMITER ;
