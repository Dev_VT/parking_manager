
CREATE TABLE Employee (
       surname              VARCHAR(20) NOT NULL,
       name                 VARCHAR(20) NOT NULL,
       id_employee          SMALLINT NOT NULL,
       patronymic           VARCHAR(20),
       position             CHAR(1) NOT NULL,
       password             VARCHAR(12) NOT NULL,
       PRIMARY KEY (id_employee)
);

CREATE UNIQUE INDEX XPKEmployee ON Employee
(
       id_employee
);

CREATE TABLE Car_parking (
       id_employee          SMALLINT NOT NULL,
       id_number            SMALLINT NOT NULL,
       title                VARCHAR(20) NOT NULL,
       district             VARCHAR(20) NOT NULL,
       street               VARCHAR(20) NOT NULL,
       PRIMARY KEY (id_number),
       FOREIGN KEY (id_employee)
                             REFERENCES Employee
);

CREATE UNIQUE INDEX XPKCar_parking ON Car_parking
(
       id_number
);

CREATE INDEX XIF2Car_parking ON Car_parking
(
       id_employee
);

CREATE TABLE Car_model (
       brand                VARCHAR(20) NOT NULL,
       model                VARCHAR(20) NOT NULL,
       id_model             SMALLINT NOT NULL,
       brand_image          BLOB,
       PRIMARY KEY (id_model)
);

CREATE UNIQUE INDEX XPKCar_model ON Car_model
(
       id_model
);

CREATE TABLE Car (
       id_number            SMALLINT NOT NULL,
       id_car_number        VARCHAR(9) NOT NULL,
       id_model             SMALLINT NOT NULL,
       color                VARCHAR(15) NOT NULL,
       mileage              INTEGER,
       snp_owner            VARCHAR(30) NOT NULL,
       PRIMARY KEY (id_car_number),
       FOREIGN KEY (id_model)
                             REFERENCES Car_model,
       FOREIGN KEY (id_number)
                             REFERENCES Car_parking
);

CREATE UNIQUE INDEX XPKCar ON Car
(
       id_car_number
);

CREATE INDEX XIF3Car ON Car
(
       id_number
);

CREATE INDEX XIF4Car ON Car
(
       id_model
);
