CREATE DATABASE IF NOT EXISTS CarParkingDB;

USE CarParkingDB;

CREATE TABLE Employee (
       id_employee          SMALLINT NOT NULL AUTO_INCREMENT,
       surname              VARCHAR(20) NOT NULL,
       name                 VARCHAR(20) NOT NULL,
       patronymic           VARCHAR(20) DEFAULT '',
       position             CHAR(1) NOT NULL,
       phone                CHAR(11) NOT NULL UNIQUE,
       password             VARCHAR(12) NOT NULL UNIQUE,
       PRIMARY KEY (id_employee)
);

CREATE UNIQUE INDEX XPKEmployee ON Employee
(
  id_employee
);

CREATE TABLE Car_parking (
       id_number            SMALLINT NOT NULL AUTO_INCREMENT,
       id_employee          SMALLINT NOT NULL,
       parking_number       SMALLINT UNSIGNED NOT NULL UNIQUE,
       title                VARCHAR(20) NOT NULL UNIQUE,
       district             VARCHAR(20) NOT NULL,
       street               VARCHAR(20) NOT NULL,
       PRIMARY KEY (id_number),
       FOREIGN KEY (id_employee)
                             REFERENCES Employee (id_employee)
                             ON DELETE RESTRICT
);

CREATE UNIQUE INDEX XPKCar_parking ON Car_parking
(
       id_number
);

CREATE UNIQUE INDEX XIF2Car_parking ON Car_parking
(
       id_employee
);

CREATE VIEW Car_parking_list
       AS SELECT p.*, CONCAT_WS(' ', e.surname, e.name, e.patronymic) AS snp, e.phone
       FROM Car_parking AS p, Employee AS e
       WHERE p.id_employee = e.id_employee;

CREATE TABLE Car_brand (
       id_brand             SMALLINT NOT NULL AUTO_INCREMENT,
       brand                VARCHAR(20) NOT NULL,
       PRIMARY KEY (id_brand)
);

CREATE UNIQUE INDEX XPKCar_brand ON Car_brand
(
       id_brand
);

CREATE TABLE Car_model (
       id_model             SMALLINT NOT NULL AUTO_INCREMENT,
       id_brand             SMALLINT NOT NULL,
       model                VARCHAR(20) NOT NULL,
       PRIMARY KEY (id_model),
       FOREIGN KEY (id_brand)
                             REFERENCES Car_brand (id_brand)
                             ON DELETE CASCADE
);

CREATE UNIQUE INDEX XPKCar_model ON Car_model
(
       id_model
);

CREATE TABLE Car (
       id_car_number        INT NOT NULL AUTO_INCREMENT,
       gov_car_number       VARCHAR(9) NOT NULL UNIQUE,
       id_model             SMALLINT NOT NULL,
       id_number            SMALLINT NOT NULL,
       color                TINYINT UNSIGNED NOT NULL,
       mileage              INT UNSIGNED CHECK (mileage > 0),
       owner_name           VARCHAR(20) NOT NULL,
       owner_surname        VARCHAR(20) NOT NULL,
       owner_patronymic     VARCHAR(20),
       phone                CHAR(11),
       PRIMARY KEY (id_car_number),
       FOREIGN KEY (id_model)
                             REFERENCES Car_model (id_model)
                             ON DELETE RESTRICT,
       FOREIGN KEY (id_number)
                             REFERENCES Car_parking (id_number)
                             ON DELETE RESTRICT
);

CREATE UNIQUE INDEX XPKCar ON Car
(
       id_car_number
);

CREATE UNIQUE INDEX XPKCarNumber ON Car
(
       gov_car_number
);

CREATE INDEX XIF3Car ON Car
(
       id_number
);

CREATE INDEX XIF4Car ON Car
(
       id_model
);

CREATE VIEW Model_list
       AS SELECT b.id_brand, brand, id_model, model
       FROM Car_model AS m, Car_brand AS b
       WHERE m.id_brand = b.id_brand;

CREATE VIEW Car_list
       AS SELECT id_car_number, gov_car_number, brand, model, color, mileage, id_number,
       CONCAT_WS(' ', owner_surname, owner_name, owner_patronymic) AS snp, owner_name, owner_surname, owner_patronymic, phone
       FROM Car AS c, Model_list AS m
       WHERE c.id_model = m.id_model;
