USE CarParkingDB;

DELIMITER $$

CREATE PROCEDURE `login_employee`(IN in_password VARCHAR(12))
BEGIN
	SELECT id_employee
    FROM Employee
    WHERE password = in_password;
END$$

CREATE PROCEDURE `has_administrator`()
BEGIN
	SELECT COUNT(id_employee)
    FROM Employee
    WHERE position = 'a'
		GROUP BY position;
END$$

CREATE PROCEDURE `employee_list`()
BEGIN
	SELECT id_employee, surname, name, patronymic, position, phone
    FROM Employee
		ORDER BY surname;
END$$

CREATE PROCEDURE `parking_list`()
BEGIN
	SELECT id_number, parking_number, title, district, street, snp, phone
    FROM Car_parking_list
		ORDER BY parking_number;
END$$

CREATE PROCEDURE `parking_swap_managers`(IN id_new_manager smallint,
	IN id_update_parking smallint, IN id_old_manager smallint, IN id_second_parking smallint)
BEGIN
	DECLARE id_admin smallint;
	SET id_admin = (
		SELECT id_employee
			FROM Employee
			WHERE position = 'a' LIMIT 1);
	UPDATE car_parking
		SET id_employee = id_admin
		WHERE id_number = id_update_parking;
	UPDATE car_parking
		SET id_employee = id_old_manager
		WHERE id_number = id_second_parking;
	UPDATE car_parking
		SET id_employee = id_new_manager
		WHERE id_number = id_update_parking;
END$$

CREATE PROCEDURE `parking_list_update_car`(IN is_manager boolean, IN id_manager smallint)
BEGIN
	IF is_manager = FALSE THEN
		SELECT id_number, parking_number, title
	    FROM Car_parking_list
			ORDER BY parking_number;
	ELSE
		SELECT id_number, parking_number, title
			FROM Car_parking_list
			WHERE id_employee = id_manager
			ORDER BY parking_number;
	END IF;
END$$

CREATE PROCEDURE `model_list`()
BEGIN
	SELECT id_brand, brand, id_model, model
    FROM Model_list
		ORDER BY brand, model;
END$$

CREATE PROCEDURE `car_list`(IN id_parking smallint, IN snp_union boolean)
BEGIN
	IF snp_union = TRUE THEN
		SELECT id_car_number, gov_car_number, brand, model, color, mileage, snp, phone
			FROM Car_list
			WHERE id_number = id_parking
			ORDER BY gov_car_number;
	ELSE
		SELECT id_car_number, gov_car_number, brand, model, color, mileage, owner_surname, owner_name, owner_patronymic, phone
			FROM Car_list
			WHERE id_number = id_parking
			ORDER BY gov_car_number;
	END IF;
END$$

CREATE PROCEDURE `search_car`(IN search_gov_car_number VARCHAR(9))
BEGIN
	SELECT id_car_number, gov_car_number, brand, model, color, mileage, snp, phone, parking_number, title
    FROM Car_list AS c, Car_parking AS p
    WHERE c.id_number = p.id_number
		AND c.gov_car_number LIKE CONCAT('%', LOWER(search_gov_car_number), '%')
		ORDER BY gov_car_number;
END$$

CREATE PROCEDURE `document_parking_list`()
BEGIN
	SELECT id_number, title, parking_number, district, street, snp, phone
    FROM Car_parking_list
		ORDER BY title;
END$$

CREATE PROCEDURE `document_parking_list_with_car_count`()
BEGIN
	SELECT p.id_number, title, parking_number, district, street, snp, p.phone, COUNT(c.id_car_number) AS car_count
    FROM Car_parking_list AS p, Car AS c
		WHERE p.id_number = c.id_number
		GROUP BY p.id_number
		ORDER BY title;
END$$

DELIMITER ;
