"""parking_manager URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from manager import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.empty_path, name='index'),
    path('login/', views.login_page, name='login_page'),
    path('login_check/', views.login_check, name='login_check'),
    path('logout/', views.logout, name='logout'),
    path('main/', views.main_page, name='main_page'),
    path('define_parking_manager_page/', views.define_parking_manager_page, name='define_parking_manager_page'),
    path('employee_list/', views.employees_list_page, name='employees_list_page'),
    path('employee/', views.employee_page, name='employee_page'),
    path('employee=<int:id_employee>/', views.employee_page, name='employee_page'),
    path('employee_create/', views.employee_create, name='employee_create'),
    path('employee_update=<int:id_employee>/', views.employee_create, name='employee_update'),
    path('employee_delete=<int:id_employee>/', views.employee_delete, name='employee_delete'),
    path('parking_list/', views.parking_list_page, name='parking_list_page'),
    path('parking_page/', views.parking_page, name='parking_page'),
    path('parking_page=<int:id_parking>/', views.parking_page, name='parking_page'),
    path('parking_create/', views.parking_create, name='parking_create'),
    path('parking_update=<int:id_parking>/', views.parking_create, name='parking_update'),
    path('parking_delete=<int:id_parking>/', views.parking_delete, name='parking_delete'),
    path('car_list_page=<int:id_parking>/', views.car_list_page, name='car_list_page'),
    path('have_not_parking/', views.have_not_parking_page, name='have_not_parking_page'),
    path('car_page_parking=<int:id_parking>/', views.car_page, name='car_page'),
    path('car_page=<int:id_car>/', views.car_page, name='car_page'),
    path('car_create_parking=<int:id_parking>/', views.car_create, name='car_create'),
    path('car_update=<int:id_car>/', views.car_create, name='car_update'),
    path('car_delete=<int:id_car>/', views.car_delete, name='car_delete'),
    path('car_search/', views.car_search_page, name='car_search_page'),
    path('documents/', include('manager.urls')),
]
