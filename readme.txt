Запуск проекта

Используемые версии Python: 3.8 или 3.9. Нужные модули указаны в requirements.txt.
Проект работает с СУБД MySQL версии 28. Следует её установить вместе с Workbench и client for python.
Перед установкой клиента обязательно сначала надо установить Python.
Чтобы работать с БД, нужно проверить что служба MySQL80 запущена.

Следует создать БД для проекта. Удобно сделать её через Workbench. Все скрипты в папке MySQL:
- script_updated.sql содержит таблицы, индексы и представления;
- script_procedures.sql содержит используемые процедуры;
- script_create_user.sql создает пользователя для Django;
- дополнительно можно создать БД для тестов, изменив в script_updated.sql дополнив имя БД приставкой test_.

В папку /manager/files/fonts/ нужно положить шрифты:
times-new-roman.ttf
times-new-roman-bold.ttf

В папку /manager/static/images/ нужно положить картинки для главного меню:
document.jpg
employees.jpg
find_car.jpg
parking_lots.jpg

Создать системные таблицы в БД для Django можно командой:
py manage.py migrate

Запустить имеющиеся тесты в тестовой БД:
py manage.py test --keepdb

Запустить проект в localhost:
py manage.py runserver

При первом входе в учетную записть главного администратора добавляются тестовые данные.
Пароли для входа можно найти в /manager/app_tests/suppliers/employee_supplier.py.
