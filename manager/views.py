from django.shortcuts import render
from django.http import HttpResponseRedirect, FileResponse
from django.urls import reverse
from manager.data.menu import MenuClass
from manager.data import employee, parking, car, documents
from manager.data.login import Login, is_login, restriction_check


def empty_path(request):
    return HttpResponseRedirect(reverse('login_page'))


@is_login
def login_page(request):
    context = _create_context(request.session)
    _show_error_message(request.session, context)
    return render(request, 'manager/login.html', context)


@is_login
def login_check(request) -> HttpResponseRedirect:
    password = request.POST['password_field']
    next_url = 'main_page'
    loginCheck = Login(password)
    if loginCheck.is_login_correct():
        employeeLoginId = loginCheck.get_login_id()
        SessionStorageEmployee.add_employee_data_in_session(
            request.session, employeeLoginId)
    else:
        next_url = 'login_page'
        _add_error_message(request.session, loginCheck.get_msg())
    return HttpResponseRedirect(reverse(next_url))


def logout(request) -> HttpResponseRedirect:
    SessionStorageEmployee.delete_employee_data_in_session(request.session)
    return HttpResponseRedirect(reverse('login_page'))


@restriction_check(employee.Employee.PARKING_MANAGER)
def main_page(request):
    context = _create_context(request.session, 1)
    SessionStorageEmployee.add_hello_label(request.session, context)
    return render(request, 'manager/main.html', context)


@restriction_check(employee.Employee.ADMIN, employee.Employee.SECRETARY)
def define_parking_manager_page(request):
    parkingManagerId = SessionStorageEmployee.get_id(request.session)
    parkingId = parking.Parking.get_parking_id_attached_parking_manager(parkingManagerId)
    if parkingId > -1:
        redirectUrl = reverse('car_list_page', kwargs={'id_parking': parkingId})
    else:
        redirectUrl = reverse('have_not_parking_page')
    return HttpResponseRedirect(redirectUrl)


@restriction_check(employee.Employee.SECRETARY, employee.Employee.PARKING_MANAGER)
def employees_list_page(request):
    context = _create_context(request.session)
    el = employee.EmployeesList()
    context['employees_list'] = el.get_list()
    context['employees_count'] = el.get_size()
    _show_error_message(request.session, context)
    return render(request, 'manager/employees_list.html', context)


@restriction_check(employee.Employee.SECRETARY, employee.Employee.PARKING_MANAGER)
def employee_page(request, id_employee: int = -1):
    context = _create_context(request.session)
    context['positions'] = employee.Employee.get_position_choices()

    if id_employee > -1:
        context['id_employee'] = id_employee
        context['title'] = 'Изменение информации о сотруднике'
        context['password'] = 'Изменение пароля'
        context['button'] = 'Сохранить'
        # отключить поле выбора должности, если администратор изменяет себя
        adminId = SessionStorageEmployee.get_id(request.session)
        if id_employee == adminId:
            context['positionDisabled'] = True
        if not 'employee_page' in request.session:
            context.update(employee.Employee.get_employee_values(id_employee))
    else:
        context['title'] = 'Добавление нового сотрудника'
        context['password'] = 'Пароль'
        context['button'] = 'Создать'
        context['employee_position'] = 's'

    if 'employee_page' in request.session:
        context.update(request.session['employee_page'])
        del request.session['employee_page']
    _show_error_message(request.session, context)

    return render(request, 'manager/employee.html', context)


@restriction_check(employee.Employee.SECRETARY, employee.Employee.PARKING_MANAGER)
def employee_create(request, id_employee: int=-1) -> HttpResponseRedirect:
    ''' Создание/обновление сотрудника '''
    if id_employee > -1:
        adminId = SessionStorageEmployee.get_id(request.session)
        e = employee.EmployeeUpdate(request.POST, id_employee, adminId)
    else:
        e = employee.EmployeeAddNew(request.POST)

    urlDict = dict()
    if e.get_status():
        nextUrl = 'employee_page' if 'go_next' in request.POST else 'employees_list_page'
    else:
        nextUrl = 'employee_page'
        request.session['employee_page'] = e.get_employee_data()
        _add_error_message(request.session, e.get_msg())
        if id_employee > -1:
            urlDict['id_employee'] = id_employee
    return HttpResponseRedirect(reverse(nextUrl, kwargs=urlDict))


@restriction_check(employee.Employee.SECRETARY, employee.Employee.PARKING_MANAGER)
def employee_delete(request, id_employee: int) -> HttpResponseRedirect:
    adminId = SessionStorageEmployee.get_id(request.session)
    e = employee.EmployeeDelete(id_employee, adminId)
    if not e.get_status():
        _add_error_message(request.session, e.get_msg())
    return HttpResponseRedirect(reverse('employees_list_page'))


@restriction_check(employee.Employee.PARKING_MANAGER)
def parking_list_page(request):
    context = _create_context(request.session)
    el = parking.ParkingList()
    context['parking_list'] = el.get_list()
    context['parking_count'] = el.get_size()
    _show_error_message(request.session, context)
    return render(request, 'manager/parking_list.html', context)


@restriction_check(employee.Employee.PARKING_MANAGER)
def parking_page(request, id_parking: int = -1):
    context = _create_context(request.session)

    if id_parking > -1:
        context['id_parking'] = id_parking
        context['title'] = 'Изменение информации о стоянке'
        context['button'] = 'Сохранить'
        managers = employee.ManagersList()
        context['employees'] = managers.get_list()
        if not 'parking_page' in request.session:
            context.update(parking.Parking.get_parking_values(id_parking))
    else:
        context['title'] = 'Добавление новой стоянки'
        context['button'] = 'Создать'
        if _add_manager_list(request, context) == 0:
            return HttpResponseRedirect(reverse('parking_list_page'))

    if 'parking_page' in request.session:
        context.update(request.session['parking_page'])
        del request.session['parking_page']
    _show_error_message(request.session, context)
    return render(request, 'manager/parking.html', context)


def _add_manager_list(request, context) -> int:
    managers = employee.ManagersList(freeManagers=True)
    context['employees'] = managers.get_list()
    if not managers.get_status():
        _add_error_message(request.session, managers.get_msg())
    return managers.get_size()


@restriction_check(employee.Employee.PARKING_MANAGER)
def parking_create(request, id_parking: int=-1) -> HttpResponseRedirect:
    ''' Создание/обновление стоянки '''
    if id_parking > -1:
        p = parking.ParkingUpdate(request.POST, id_parking)
    else:
        p = parking.ParkingAddNew(request.POST)

    urlDict = dict()
    if p.get_status():
        nextUrl = 'parking_page' if 'go_next' in request.POST else 'parking_list_page'
    else:
        nextUrl = 'parking_page'
        request.session['parking_page'] = p.get_parking_data()
        _add_error_message(request.session, p.get_msg())
        if id_parking > -1:
            urlDict['id_parking'] = id_parking
    return HttpResponseRedirect(reverse(nextUrl, kwargs=urlDict))


@restriction_check(employee.Employee.PARKING_MANAGER)
def parking_delete(request, id_parking: int) -> HttpResponseRedirect:
    p = parking.ParkingDelete(id_parking)
    if not p.get_status():
        _add_error_message(request.session, p.get_msg())
    return HttpResponseRedirect(reverse('parking_list_page'))


@restriction_check()
def car_list_page(request, id_parking: int):
    employeePosition = SessionStorageEmployee.get_position(request.session)
    openedMenuItem = 1 if employeePosition == employee.Employee.PARKING_MANAGER else -1
    context = _create_context(request.session, openedMenuItem)
    SessionStorageEmployee.add_hello_label(request.session, context)
    cl = car.CarList(id_parking)
    context['car_list'] = cl.get_list()
    context['cars_count'] = cl.get_size()
    context['parking_id'] = id_parking
    context['parking'] = parking.Parking.get_parking_values(id_parking)
    _show_error_message(request.session, context)
    return render(request, 'manager/car_list.html', context)


@restriction_check(employee.Employee.ADMIN, employee.Employee.SECRETARY)
def have_not_parking_page(request):
    context = _create_context(request.session, 1)
    return render(request, 'manager/have_not_parking.html', context)


@restriction_check()
def car_page(request, id_parking: int=-1, id_car: int = -1):
    context = _create_context(request.session)
    context['color_list'] = car.Car.CAR_COLOURS
    context['brand_dict'] = car.Car.get_car_brands()

    if id_car > -1:
        context['id_car'] = id_car
        context['title'] = 'Изменение информации об автомобиле'
        context['button'] = 'Сохранить'
        idEmployee = SessionStorageEmployee.get_id(request.session)
        employeePosition = SessionStorageEmployee.get_position(request.session)
        context['parking_list'] = parking.ParkingList.get_list_for_car(employeePosition, idEmployee)
        if not 'car_page' in request.session:
            context.update(car.Car.get_car_values(id_car))
    else:
        context['title'] = 'Добавление нового автомобиля'
        context['button'] = 'Создать'
        context['parking'] = parking.Parking.get_parking_values(id_parking)
        context['id_parking'] = id_parking

    if 'car_page' in request.session:
        context.update(request.session['car_page'])
        del request.session['car_page']
    _show_error_message(request.session, context)
    return render(request, 'manager/car.html', context)


@restriction_check()
def car_create(request, id_parking: int=-1, id_car: int=-1) -> HttpResponseRedirect:
    ''' Создание/обновление автомобиля '''
    if id_car > -1:
        c = car.CarUpdate(request.POST, carId=id_car)
    else:
        c = car.CarAddNew(request.POST, parkingId=id_parking)

    urlDict = dict()
    if c.get_status():
        nextUrl = 'car_page' if 'go_next' in request.POST else 'car_list_page'
        urlDict['id_parking'] = c.get_parking_id()
    else:
        nextUrl = 'car_page'
        carData = c.get_car_data()
        if len(carData) > 0:
            request.session['car_page'] = carData
        if id_car > -1:
            urlDict['id_car'] = id_car
        else:
            urlDict['id_parking'] = id_parking
        _add_error_message(request.session, c.get_msg())
    return HttpResponseRedirect(reverse(nextUrl, kwargs=urlDict))


@restriction_check()
def car_delete(request, id_car: int) -> HttpResponseRedirect:
    c = car.CarDelete(id_car)
    if c.get_status():
        urlDict = {'id_parking': c.get_parking_id()}
        return HttpResponseRedirect(reverse('car_list_page', kwargs=urlDict))
    else:
        _add_error_message(request.session, c.get_msg())
        return HttpResponseRedirect(reverse('parking_list_page'))


@restriction_check(employee.Employee.PARKING_MANAGER)
def car_search_page(request, searchStr: str=''):
    context = _create_context(request.session)
    if 'search_field' in request.GET:
        search = car.SearchCar(request.GET)
        if search.get_status():
            context.update(search.get_search_data())
            context['status_ok'] = True
            context['car_list'] = search.get_list()
            context['cars_count'] = search.get_count()
        else:
            _add_error_message(request.session, search.get_msg())
            _show_error_message(request.session, context)
    return render(request, 'manager/car_list_search.html', context)


@restriction_check(employee.Employee.PARKING_MANAGER)
def document_parking_list(request):
    document = documents.DocumentParkingList()
    document.create_pdf()
    fileBuffer = document.get_pdf()
    fileName = document.get_file_name()
    return FileResponse(fileBuffer, as_attachment=True, filename=fileName)


@restriction_check()
def document_car_list(request, id_parking: int):
    document = documents.DocumentCarList(id_parking)
    document.create_pdf()
    fileBuffer = document.get_pdf()
    fileName = document.get_file_name()
    return FileResponse(fileBuffer, as_attachment=True, filename=fileName)


@restriction_check(employee.Employee.PARKING_MANAGER)
def document_all_car_list(request):
    document = documents.DocumentAllCarList()
    document.create_pdf()
    fileBuffer = document.get_pdf()
    fileName = document.get_file_name()
    return FileResponse(fileBuffer, as_attachment=True, filename=fileName)


def _create_context(session: dict, openedMenuItem: int = -1):
    '''
    Создание словаря, содержащего данные для шаблона html.
    В методе добавляются пункты меню и данные о текущем сотруднике.
    '''
    employee = {
        'position': None,
        'surname_name': '',
    }
    if SessionStorageEmployee.has_employee(session):
        employee['position'] = SessionStorageEmployee.get_position(session)
        employee['surname_name'] = SessionStorageEmployee.get_surname_and_name(session)

    context = {
        MenuClass.STR_CONTENT: MenuClass.get_menu(employee['position']),
        MenuClass.STR_OPEN: openedMenuItem,
        'employee_surname_name': employee['surname_name'],
        'employee_position': employee['position'],
    }
    return context


def _add_error_message(session: dict, msg):
    '''Добавить сообщение об ошибке в словарь сессии'''
    session['error_msg'] = msg


def _show_error_message(session: dict, context: dict):
    '''Показать добавленное сообщение об ошибке на выводимой странице'''
    if 'error_msg' in session:
        context['error_msg'] = session['error_msg']
        del session['error_msg']


class SessionStorageEmployee():
    def has_employee(session):
        return 'employee' in session


    def add_employee_data_in_session(session, employeeId):
        employeeDict = employee.Employee.get_employee_values(employeeId, withoutPhone=True)
        employeeDict['employee_id'] = employeeId
        session['employee'] = employeeDict
        session['login_success'] = True


    def add_hello_label(session, context):
        if 'login_success' in session:
            context['login_success'] = True
            del session['login_success']


    def get_id(session):
        return session['employee']['employee_id']


    def get_position(session):
        return session['employee']['employee_position']


    def get_surname_and_name(session):
        e = session['employee']
        eName = e[employee.EmployeeCheckAbstract.EMPLOYEE_VALUES[employee.INDEX_NAME]]
        eSurname = e[employee.EmployeeCheckAbstract.EMPLOYEE_VALUES[employee.INDEX_SURNAME]]
        return eSurname + ' ' + eName


    def delete_employee_data_in_session(session):
        if 'employee' in session:
            del session['employee']
