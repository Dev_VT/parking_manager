from django.core.exceptions import ObjectDoesNotExist
from manager.data.errors_check import ErrorsCheckFieldsAbstract
from manager.models import CarBrand, CarModel


INDEX_NEW_MODEL = 0
INDEX_NEW_BRAND = 1
INDEX_NEW_MODEL_RADIO = 2


class ModelAddNew(ErrorsCheckFieldsAbstract):
    ''' Добавить новую модель и марку '''
    MODEL_FIELDS = (
        ('1_model_field',
        '1_brand_field'),
        ('2_model_field',
        '2_brand_field'),
        'new_model_radio',
    )
    NEW_MODEL_RADIO = {
        'old_model': '0',
        'new_model': '1',
        'new_brand': '2',
    }
    MODEL_VALUES = (
        'car_model',
        'car_brand',
    )
    ERR_STATUS_CHECK = {
        'error_not_int': 20,
        'error_brand_not_found': 21,
        'error_model_already_exists': 22,
    }


    def __init__(self, request_POST: dict, checkAndReadNow: bool=True, addNow: bool=True):
        super().__init__()
        self.ERR_STATUS.update(self.ERR_STATUS_CHECK)
        if ModelAddNew.need_add_new_model(request_POST):
            self.addNewBrand = self._need_add_new_brand(request_POST)
            if checkAndReadNow:
                self.read_brand_data(request_POST)
                self.check_fields()
                if addNow and self.statusOk:
                    self.add_new_model()
        else:
            self.statusOk = False
            self._set_error_message(self.ERR_STATUS['error_field'],
                'Команда добавления модели не выбрана.')


    def need_add_new_model(request_POST: dict) -> bool:
        ''' Проверить, нужно ли добавлять новую модель '''
        newModelRadioField = ModelAddNew.MODEL_FIELDS[INDEX_NEW_MODEL_RADIO]
        if newModelRadioField in request_POST:
            radioValue = request_POST[newModelRadioField]
            if (radioValue == ModelAddNew.NEW_MODEL_RADIO['new_model'] or
                radioValue == ModelAddNew.NEW_MODEL_RADIO['new_brand']):
                return True
        return False


    def _need_add_new_brand(self, request_POST: dict):
        newModelRadioField = self.MODEL_FIELDS[INDEX_NEW_MODEL_RADIO]
        radioValue = request_POST[newModelRadioField]
        return True if radioValue == self.NEW_MODEL_RADIO['new_brand'] else False


    def read_brand_data(self, request_POST: dict):
        modelFields = self.MODEL_FIELDS[1] if self.addNewBrand else self.MODEL_FIELDS[0]
        self.modelData = {
            self.MODEL_VALUES[
                modelFields.index(k)]: v
            for k, v in request_POST.items()
            if k in modelFields
        }


    def check_fields(self):
        self.statusOk = False
        if self._has_not_empty_fields():
            self.selectedBrand = None
            if self.addNewBrand:
                self._brand_not_repeat()
                if self._model_already_exists():
                    self.statusOk = True
            else:
                if self._fields_be_int():
                    if self._change_id_to_brand():
                        if self._model_already_exists():
                            self.statusOk = True


    def _has_not_empty_fields(self) -> bool:
        NOT_EMPTY_FIELDS = (INDEX_NEW_MODEL,INDEX_NEW_BRAND)
        try:
            for i in NOT_EMPTY_FIELDS:
                if self.modelData[self.MODEL_VALUES[i]] == '':
                    self._set_error_message(self.ERR_STATUS['error_field'], 'Не все поля заполнены!')
                    return False
        except KeyError:
            self._set_error_message(self.ERR_STATUS['error_field'], 'Данные не были переданы.')
            return False
        return True


    def _fields_be_int(self):
        INT_FIELDS = (INDEX_NEW_BRAND,)
        try:
            for i in INT_FIELDS:
                k = self.MODEL_VALUES[i]
                self.modelData[k] = int(self.modelData[k])
                if self.modelData[k] < 0:
                    raise ValueError
        except ValueError:
            self._set_error_message(self.ERR_STATUS['error_not_int'],
                'Марка автомобиля выбрана неверно.')
            return False
        return True


    def _change_id_to_brand(self):
        try:
            idBrand = self.modelData[self.MODEL_VALUES[INDEX_NEW_BRAND]]
            self.selectedBrand = CarBrand.objects.get(pk=idBrand)
        except ObjectDoesNotExist:
            self._set_error_message(self.ERR_STATUS['error_brand_not_found'],
                'Указанная марка автомобилей не найдена.')
            return False
        return True


    def _model_already_exists(self):
        ''' Есть ли такая модель у марки '''
        if self.selectedBrand != None:
            strModel = self.modelData[self.MODEL_VALUES[INDEX_NEW_MODEL]]
            hasModel = self.selectedBrand.carmodel_set.filter(model__iexact=strModel.lower())
            if hasModel.count() > 0:
                self._set_error_message(self.ERR_STATUS['error_model_already_exists'],
                    'Данная модель уже существует.')
                return False
        return True


    def _brand_not_repeat(self):
        ''' Если такая марка есть, не добавлять дубликат '''
        strBrand = self.modelData[self.MODEL_VALUES[INDEX_NEW_BRAND]]
        hasBrand = CarBrand.objects.filter(brand__iexact=strBrand.lower())
        if hasBrand.count() > 0:
            self.selectedBrand = hasBrand[0]


    def add_new_model(self):
        md = {k.split(sep='_',maxsplit=1)[1]: v for k, v in self.modelData.items()}
        if self.addNewBrand:
            brand = self._add_new_brand(md['brand'])
        else:
            brand = self.selectedBrand
        self.newModel = CarModel(model = md['model'], id_brand=brand)
        self.newModel.save()


    def _add_new_brand(self, brandTitle):
        if self.selectedBrand != None:
            brand = self.selectedBrand
        else:
            brand = CarBrand(brand=brandTitle)
            brand.save()
        return brand


    def get_model_data(self) -> dict:
        newModelRadioField = self.MODEL_FIELDS[INDEX_NEW_MODEL_RADIO]
        if self.addNewBrand:
            self.modelData[newModelRadioField] = self.NEW_MODEL_RADIO['new_brand']
        else:
            self.modelData[newModelRadioField] = self.NEW_MODEL_RADIO['new_model']
        return self.modelData


    def get_added_model(self):
        return self.newModel


    def get_car_model_model(self):
        return self.newModel


    def brand_find(brandStr: str) -> CarBrand:
        return CarBrand.objects.get(brand=brandStr)


    def find_model(modelStr: str) -> CarModel:
        return CarModel.objects.get(model=modelStr)
