from django.core.exceptions import ObjectDoesNotExist
from manager.data.db_connection import call_procedure, call_procedure_with_args
from manager.data.errors_check import ErrorsCheckAbstract, ErrorsCheckFieldsAbstract
from manager.data.errors_check import this_field_not_updated, UPDATE_PARKING
from manager.data import employee
from manager import models


INDEX_PARKING_NUMBER = 0
INDEX_TITLE = 1
INDEX_DISTRICT = 2
INDEX_STREET = 3
INDEX_MANAGER = 4


class Parking:
    def get_parking_values(parkingId: int) -> dict:
        p = models.CarParking.objects.get(pk=parkingId)
        dataValues = ParkingCheckAbstract.PARKING_VALUES
        parkingDict = {
            dataValues[INDEX_PARKING_NUMBER]: p.parking_number,
            dataValues[INDEX_TITLE]: p.title,
            dataValues[INDEX_DISTRICT]: p.district,
            dataValues[INDEX_STREET]: p.street,
            dataValues[INDEX_MANAGER]: p.id_employee.id_employee,
        }
        return parkingDict


    def get_parking_values_in_strings(parkingId: int) -> dict:
        parkingDict = Parking.get_parking_values(parkingId)
        return {k: str(v) for k, v in parkingDict.items()}


    def get_parking_id_attached_parking_manager(parkingManagerId: int) -> int:
        parkingId = 0
        try:
            p = models.CarParking.objects.get(id_employee=parkingManagerId)
            parkingId = p.id_number
        except ObjectDoesNotExist:
            parkingId = -1
        return parkingId


class ParkingList:
    PROCEDURE_NAME = 'parking_list'


    def __init__(self):
        parkingTuple = self._get_parking_list()
        self._parkingList = self._list_transformation(parkingTuple)
        self._listSize = len(self._parkingList)


    def _get_parking_list(self) -> tuple:
        pl = call_procedure(self.PROCEDURE_NAME)
        return pl


    def _list_transformation(self, parkingTuple: tuple):
        updatedParkingList = list()
        for p in parkingTuple:
            p = list(p)
            p[6] = employee.EmployeesList.phone_transformation(p[6])
            updatedParkingList.append(p)
        return updatedParkingList


    def get_list(self):
        return self._parkingList


    def get_size(self):
        return self._listSize


    def get_list_for_car(employeePosition: str, idEmployee: int):
        parkingList = call_procedure_with_args('parking_list_update_car',
            employeePosition == employee.Employee.PARKING_MANAGER,
            idEmployee)
        parkingList = {
            p[0]: '№'+str(p[1])+' '+p[2]
            for p in parkingList
        }
        return parkingList


class ParkingCheckAbstract(ErrorsCheckFieldsAbstract):
    PARKING_FIELDS = (
        'number_field',
        'title_field',
        'district_field',
        'street_field',
        'id_employee_field',
    )
    PARKING_VALUES = (
        'parking_parking_number',
        'parking_title',
        'parking_district',
        'parking_street',
        'parking_id_employee',
    )
    ERR_STATUS_CHECK = {
        'error_not_int': 3,
        'error_number_used': 4,
        'error_title_used': 5,
        'error_employee_find': 6,
        'error_employee_position': 7,
        'error_employee_used': 8,
    }
    NUMBER_AUTOINSERT = 0


    def __init__(self, id_parking: int=-1):
        super().__init__()
        self.ERR_STATUS.update(self.ERR_STATUS_CHECK)
        self.selectedEmployee = None
        self._variablesForUpdate(id_parking)


    def _variablesForUpdate(self, id_parking):
        self.id_parking = id_parking
        self.updateParking = None
        self.fieldsNotChanged = None


    def read_parking_data(self, request_POST: dict):
        self.parkingData = {
            self.PARKING_VALUES[
                self.PARKING_FIELDS.index(k)]: v
            for k, v in request_POST.items()
            if k in self.PARKING_FIELDS
        }
        idManagerField = self.PARKING_VALUES[INDEX_MANAGER]
        if not idManagerField in self.parkingData:
            self.parkingData[idManagerField] = ''


    def check_fields(self):
        self.statusOk = False
        if self._has_not_empty_fields():
            if self._fields_be_int():
                if self._number_unique():
                    if self._title_unique():
                        if self._not_need_check_employee():
                            self.statusOk = True
                        elif self._change_id_to_employee():
                            if self._employee_is_parking_manager():
                                if self._employee_not_used():
                                    self.statusOk = True


    def _has_not_empty_fields(self) -> bool:
        NOT_EMPTY_FIELDS = (1,2,3,4)
        try:
            for i in NOT_EMPTY_FIELDS:
                if self.parkingData[self.PARKING_VALUES[i]] == '':
                    self._set_error_message(self.ERR_STATUS['error_field'], 'Не все поля заполнены!')
                    return False
        except KeyError:
            self._set_error_message(self.ERR_STATUS['error_field'], 'Данные не были переданы.')
            return False
        return True


    def _fields_be_int(self):
        number = self.parkingData[self.PARKING_VALUES[INDEX_PARKING_NUMBER]]
        INT_FIELDS = (0,4,) if number != '' else (4,)
        try:
            for i in INT_FIELDS:
                k = self.PARKING_VALUES[i]
                self.parkingData[k] = int(self.parkingData[k])
                if self.parkingData[k] <= self.NUMBER_AUTOINSERT:
                    raise ValueError
        except ValueError:
            self._set_error_message(self.ERR_STATUS['error_not_int'], 'Номер введен неверно.')
            return False
        return True


    @this_field_not_updated(UPDATE_PARKING, INDEX_PARKING_NUMBER)
    def _number_unique(self):
        number = self.parkingData[self.PARKING_VALUES[INDEX_PARKING_NUMBER]]
        if number != '':
            parkingList = models.CarParking.objects.filter(parking_number=number)
            if parkingList.count() > 0:
                self._set_error_message(self.ERR_STATUS['error_number_used'], 'Указанный номер уже связан с другой стоянкой!')
                return False
        else:
            self.parkingData[self.PARKING_VALUES[INDEX_PARKING_NUMBER]] = self.NUMBER_AUTOINSERT
        return True


    @this_field_not_updated(UPDATE_PARKING, INDEX_TITLE)
    def _title_unique(self):
        title = self.parkingData[self.PARKING_VALUES[INDEX_TITLE]]
        parkingList = models.CarParking.objects.filter(title=title)
        if parkingList.count() > 0:
            self._set_error_message(self.ERR_STATUS['error_title_used'], 'Указанное название уже принадлежит другой стоянке!')
            return False
        return True


    @this_field_not_updated(UPDATE_PARKING, INDEX_MANAGER)
    def _not_need_check_employee(self):
        ''' Декоратор вернет True если управляющий не был обновлен '''
        return False


    def _change_id_to_employee(self):
        try:
            idEmployee = self.parkingData[self.PARKING_VALUES[INDEX_MANAGER]]
            self.selectedEmployee = models.Employee.objects.get(pk=idEmployee)
        except ObjectDoesNotExist:
            self._set_error_message(self.ERR_STATUS['error_employee_find'], 'Сотрудник не найден.')
            return False
        return True


    def _employee_is_parking_manager(self):
        if self.selectedEmployee.position != employee.Employee.PARKING_MANAGER:
            self._set_error_message(self.ERR_STATUS['error_employee_position'], 'Сотрудник не является управляющим.')
            return False
        return True


    def _employee_not_used(self):
        if self._employee_swap():
            return True
        try:
            self.selectedEmployee.carparking # при отсутствии стоянки сгенерируется исключение
            self._set_error_message(self.ERR_STATUS['error_employee_used'], 'Управляющий уже занят, создайте или используйте другого.')
        except ObjectDoesNotExist:
            return True
        return False


    def _employee_swap(self):
        ''' Если происходит обновление, то управляющие могут поменяться парковками '''
        if self.updateParking != None:
            return True
        return False


    def get_parking_data(self) -> dict:
        return self.parkingData


class ParkingAddNew(ParkingCheckAbstract):
    def __init__(self, request_POST: dict):
        super().__init__()
        self.read_parking_data(request_POST)
        self.check_fields()
        if self.statusOk:
            self._add()


    def _add(self):
        self.parkingData[self.PARKING_VALUES[INDEX_MANAGER]] = self.selectedEmployee
        pd = {k.split(sep='_',maxsplit=1)[1]: v for k, v in self.parkingData.items()}
        self.newParking = models.CarParking(**pd)
        self.newParking.save()
        if self.parkingData[self.PARKING_VALUES[INDEX_PARKING_NUMBER]] == self.NUMBER_AUTOINSERT:
            self.newParking.parking_number = self.newParking.id_number
            self.newParking.save()


    def get_parking_model(self) -> models.CarParking:
        return self.newParking


class ParkingUpdate(ParkingCheckAbstract):
    def __init__(self, request_POST: dict, id_parking: int):
        super().__init__(id_parking)
        self.read_parking_data(request_POST)
        self.updateParking = Parking.get_parking_values_in_strings(id_parking)
        self.fieldsNotChanged = self._get_fields_not_changed()
        self.check_fields()
        if self.statusOk:
            self.updateParking = models.CarParking.objects.get(pk=id_parking)
            self._update()


    def _get_fields_not_changed(self) -> set:
        ''' Значения каких полей не были изменены '''
        fieldsNotChanged = set()
        for k, v in self.parkingData.items():
            if self.updateParking[k] == str(v):
                fieldsNotChanged.add(k)
        return fieldsNotChanged


    def _update(self):
        pd = {
            k.split(sep='_',maxsplit=1)[1]: v
            for k, v in self.parkingData.items()
            if k not in self.fieldsNotChanged
        }
        if 'parking_number' in pd:
            self.updateParking.parking_number = pd['parking_number']
        if 'title' in pd:
            self.updateParking.title = pd['title']
        if 'district' in pd:
            self.updateParking.district = pd['district']
        if 'street' in pd:
            self.updateParking.street = pd['street']
        if 'id_employee' in pd:
            if hasattr(self.selectedEmployee, 'carparking'):
                self._swap_managers(self.selectedEmployee, self.updateParking.id_employee)
            self.updateParking.id_employee = self.selectedEmployee
        self.updateParking.save()


    def _swap_managers(self, newManager, oldManager):
        ''' Поменять управляющих местами '''
        secondParking = newManager.carparking
        call_procedure_with_args('parking_swap_managers',
            newManager.id_employee,
            self.updateParking.id_number,
            oldManager.id_employee,
            secondParking.id_number)


class ParkingDelete(ErrorsCheckAbstract):
    ERR_STATUS_DELETE = {
        'error_parking_find': 10,
        'error_has_cars': 11,
    }


    def __init__(self, id: int):
        super().__init__()
        self.ERR_STATUS.update(self.ERR_STATUS_DELETE)
        self.idParking = id
        self._check()
        if self.statusOk:
            self._delete()


    def _check(self):
        self.statusOk = False
        if self._id_parking_right():
            if self._has_not_cars():
                self.statusOk = True


    def _id_parking_right(self):
        try:
            self.parking = models.CarParking.objects.get(pk=self.idParking)
        except ObjectDoesNotExist:
            self._set_error_message(self.ERR_STATUS['error_parking_find'], 'Стоянка не найдена.')
            return False
        return True


    def _has_not_cars(self) -> bool:
        ''' В удаляемой стоянке не должно быть автомобилей '''
        cars = self.parking.car_set.all()
        if len(cars) > 0:
            self._set_error_message(self.ERR_STATUS['error_has_cars'],
                'В стоянке содержатся автомобили. Удалите их или переместите на другую стоянку.')
            return False
        return True


    def _delete(self):
        self.parking.delete()
