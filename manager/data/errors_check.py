from functools import wraps


class ErrorsCheckAbstract:
    ERR_STATUS = {
        'not_errors': 0,
        'error_database': 1, # Ошибка произошла в БД
    }


    def __init__(self):
        self.err_status = ErrorsCheckAbstract.ERR_STATUS['not_errors']
        self.statusOk = False


    def _set_error_message(self, err_status: int, err_msg: str):
        self.err_status = err_status
        self.error_msg = err_msg


    def get_msg(self) -> str:
        return self.error_msg


    def get_status(self) -> bool:
        return self.statusOk


class ErrorsCheckFieldsAbstract(ErrorsCheckAbstract):
    ERR_STATUS_FIELDS = {
        'error_field': 2, # Ошибка произошла при проверке введенных данных
    }

    def __init__(self):
        super().__init__()
        self.ERR_STATUS.update(self.ERR_STATUS_FIELDS)


UPDATE_EMPLOYEE = 30
UPDATE_PARKING = 40
UPDATE_CAR = 50


def this_field_not_updated(whichClass: int, numberField: int):
    def is_updated_check(func):
        ''' Декоратор определяет нужно ли выполнять проверку поля при обновлении данных '''
        @wraps(func)
        def wrapper(*args, **kwargs):
            isSelf = args[0]
            if whichClass == UPDATE_EMPLOYEE:
                field = isSelf.EMPLOYEE_VALUES[numberField]
            elif whichClass == UPDATE_PARKING:
                field = isSelf.PARKING_VALUES[numberField]
            elif whichClass == UPDATE_CAR:
                field = isSelf.CAR_VALUES[numberField]

            if (isSelf.fieldsNotChanged != None and
                field in isSelf.fieldsNotChanged):
                return True
            return func(*args, **kwargs)
        return wrapper
    return is_updated_check
