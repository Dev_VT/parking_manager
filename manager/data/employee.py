from manager.data.db_connection import call_procedure
from manager.data.errors_check import ErrorsCheckAbstract, ErrorsCheckFieldsAbstract
from manager.data.errors_check import this_field_not_updated, UPDATE_EMPLOYEE
from manager.models import Employee as EmployeeModel


INDEX_NAME = 0
INDEX_SURNAME = 1
INDEX_PATRONYMIC = 2
INDEX_POSITION = 3
INDEX_PHONE = 4
INDEX_PASSWORD = 5
INDEX_REPEAT_PASSWORD = 6


class Employee:
    ADMIN = EmployeeModel.positionChoices[0][0]
    SECRETARY = EmployeeModel.positionChoices[1][0]
    PARKING_MANAGER = EmployeeModel.positionChoices[2][0]


    def get_employee_values(employeeId: int, withoutPhone: bool=False) -> dict:
        e = EmployeeModel.objects.get(pk=employeeId)
        dataValues = EmployeeCheckAbstract.EMPLOYEE_VALUES
        employeeDict = {
            dataValues[INDEX_NAME]: e.name,
            dataValues[INDEX_SURNAME]: e.surname,
            dataValues[INDEX_PATRONYMIC]: e.patronymic,
            dataValues[INDEX_POSITION]: e.position,
        }
        if not withoutPhone:
            employeeDict[dataValues[INDEX_PHONE]] = e.phone
        return employeeDict


    def get_position_choices():
        return EmployeeModel.positionChoices


class EmployeesList:
    def __init__(self):
        employeeTuple = self._get_employees_list()
        self._employeesList = self._list_transformation(employeeTuple)
        self._listSize = len(self._employeesList)


    def _get_employees_list(self) -> tuple:
        el = call_procedure('employee_list')
        return el


    def _list_transformation(self, employeeTuple: tuple):
        positions = Employee.get_position_choices()
        positions = {pos[0]: pos[1] for pos in positions} # преобразование кортежей в словарь
        updatedEmployeeList = list()
        for e in employeeTuple:
            e = list(e)
            EmployeesList.position_names_transformation(e, positions)
            e[5] = EmployeesList.phone_transformation(e[5])
            updatedEmployeeList.append(e)
        return updatedEmployeeList


    def position_names_transformation(employeeList: tuple, positions: dict) -> list:
        employeeList[4] = positions[employeeList[4]]


    def phone_transformation(phone: str):
        phone = phone[0] + '(' + phone[1:4] + ')' + phone[4:7] + '-' + phone[7:9] + '-' + phone[9:11]
        if phone[0] == '7':
            phone = '+' + phone
        return phone


    def get_list(self):
        return self._employeesList


    def get_size(self):
        return self._listSize


class EmployeeCheckAbstract(ErrorsCheckFieldsAbstract):
    EMPLOYEE_FIELDS = (
        'name_field',
        'surname_field',
        'patronymic_field',
        'position_field',
        'phone_field',
        'password_field',
        'password_field_2'
    )
    EMPLOYEE_VALUES = (
        'employee_name',
        'employee_surname',
        'employee_patronymic',
        'employee_position',
        'employee_phone',
        'employee_password',
        'employee_password_2'
    )
    ERR_STATUS_CHECK = {
        'error_position': 3,
        'error_phone': 4,
        'error_phone_used': 5,
        'error_not_equal_passwords': 6,
        'error_length_password': 7,
        'error_password_used': 8,
    }


    def __init__(self, id: int=-1):
        super().__init__()
        self.ERR_STATUS.update(self.ERR_STATUS_CHECK)
        self._variablesForUpdate(id)


    def _variablesForUpdate(self, id_employee):
        self.id_employee = id_employee
        self.fieldsNotChanged = None


    def read_employee_data(self, request_POST: dict):
        self.employeeData = {
            self.EMPLOYEE_VALUES[self.EMPLOYEE_FIELDS.index(k)]: v
            for k, v in request_POST.items()
            if k in self.EMPLOYEE_FIELDS
        }


    def check_fields(self):
        self.statusOk = False
        if self._has_not_empty_fields():
            if self._position_right():
                if self._phone_verification():
                    if self._phone_unique():
                        if self._not_need_check_password():
                            self.statusOk = True
                        elif self._has_equal_passwords():
                            if self._password_verification():
                                if self._password_unique():
                                    self.statusOk = True
        self._delete_password_from_dict()


    def _has_not_empty_fields(self) -> bool:
        NOT_EMPTY_FIELDS = (0,1,3,4) if self.id_employee > -1 else (0,1,3,4,5,6)
        try:
            for i in NOT_EMPTY_FIELDS:
                if self.employeeData[self.EMPLOYEE_VALUES[i]] == '':
                    self._set_error_message(self.ERR_STATUS['error_field'], 'Не все поля заполнены!')
                    return False
        except KeyError:
            self._set_error_message(self.ERR_STATUS['error_field'], 'Данные не были переданы.')
            return False
        return True


    @this_field_not_updated(UPDATE_EMPLOYEE, INDEX_POSITION)
    def _position_right(self) -> bool:
        position = self.employeeData[self.EMPLOYEE_VALUES[INDEX_POSITION]]
        rightPositions = (
            Employee.ADMIN,
            Employee.SECRETARY,
            Employee.PARKING_MANAGER,
        )
        if not position in rightPositions:
            self._set_error_message(self.ERR_STATUS['error_position'], 'Должность указана неверно.')
            return False
        return True


    @this_field_not_updated(UPDATE_EMPLOYEE, INDEX_PHONE)
    def _phone_verification(self):
        phone = self.employeeData[self.EMPLOYEE_VALUES[INDEX_PHONE]]
        if phone[0] == '+':
            phone = phone[1:len(phone)]
        phoneLenght = len(phone)
        if phoneLenght == 11 and phone.isdigit():
            self.employeeData[self.EMPLOYEE_VALUES[INDEX_PHONE]] = phone
            return True
        self._set_error_message(self.ERR_STATUS['error_phone'], 'Номер телефона указан неверно.')
        return False


    @this_field_not_updated(UPDATE_EMPLOYEE, INDEX_PHONE)
    def _phone_unique(self):
        phone = self.employeeData[self.EMPLOYEE_VALUES[INDEX_PHONE]]
        employeeList = EmployeeModel.objects.filter(phone=phone)
        if employeeList.count() > 0:
            self._set_error_message(self.ERR_STATUS['error_phone_used'], 'Данный номер телефона уже используется!')
            return False
        return True


    @this_field_not_updated(UPDATE_EMPLOYEE, INDEX_PASSWORD)
    def _not_need_check_password(self):
        ''' Декоратор вернет True если пароль не был обновлен '''
        return False


    def _has_equal_passwords(self) -> bool:
        password1 = self.employeeData[self.EMPLOYEE_VALUES[INDEX_PASSWORD]]
        password2 = self.employeeData[self.EMPLOYEE_VALUES[INDEX_REPEAT_PASSWORD]]
        if password1 != password2:
            self._set_error_message(self.ERR_STATUS['error_not_equal_passwords'], 'Пароли не совпадают!')
            return False
        return True


    def _password_verification(self) -> bool:
        password = self.employeeData[self.EMPLOYEE_VALUES[INDEX_PASSWORD]]
        password_length = len(password)
        if password_length < 6 or password_length > 12:
            self._set_error_message(self.ERR_STATUS['error_length_password'], 'Длина пароля должна быть от 6 до 12 символов.')
            return False
        return True


    def _password_unique(self) -> bool:
        password = self.employeeData[self.EMPLOYEE_VALUES[INDEX_PASSWORD]]
        employeeList = EmployeeModel.objects.filter(password=password)
        if employeeList.count() > 0:
            self._set_error_message(self.ERR_STATUS['error_password_used'], 'Данный пароль уже используется!')
            return False
        return True


    def _delete_password_from_dict(self):
        if len(self.employeeData) == len(self.EMPLOYEE_VALUES):
            del self.employeeData[self.EMPLOYEE_VALUES[INDEX_REPEAT_PASSWORD]]
            if not self.statusOk:
                del self.employeeData[self.EMPLOYEE_VALUES[INDEX_PASSWORD]]


    def get_employee_data(self) -> dict:
        return self.employeeData


class EmployeeAddNew(EmployeeCheckAbstract):
    def __init__(self, request_POST: dict):
        super().__init__()
        self.read_employee_data(request_POST)
        self.check_fields()
        if self.statusOk:
            self._add()


    def _add(self):
        ed = {k.split(sep='_',maxsplit=1)[1]: v for k, v in self.employeeData.items()}
        self.newEmployee = EmployeeModel(**ed)
        self.newEmployee.save()


    def get_employee_id(self) -> int:
        return self.newEmployee.id_employee


    def get_employee_model(self) -> EmployeeModel:
        return self.newEmployee


class EmployeeUpdate(EmployeeCheckAbstract):
    ERR_STATUS_UPDATE = {
        'error_admin_position': 20,
        'error_manager_position': 21,
    }


    def __init__(self, request_POST: dict, id: int, current_id: int):
        super().__init__(id)
        self.ERR_STATUS.update(self.ERR_STATUS_UPDATE)
        self.read_employee_data(request_POST)
        self.updateEmployee = Employee.get_employee_values(id)
        self._set_admin_position(self.updateEmployee)
        self.fieldsNotChanged = self._get_fields_not_changed()
        self.check_fields()
        if self.statusOk:
            self.updateEmployee = EmployeeModel.objects.get(pk=id)
            self._update_check(current_id)
            if self.statusOk:
                self._update()


    def _set_admin_position(self, updateEmployee: dict):
        # поле выбора должности у админа заблокировано,
        # поэтому оно не может быть передано
        positionKey = self.EMPLOYEE_VALUES[INDEX_POSITION]
        if positionKey not in self.employeeData:
            self.employeeData[positionKey] = updateEmployee[positionKey]


    def _get_fields_not_changed(self) -> set:
        ''' Значения каких полей не были изменены '''
        passwordField = self.EMPLOYEE_VALUES[INDEX_PASSWORD]
        repeatPasswordField = self.EMPLOYEE_VALUES[INDEX_REPEAT_PASSWORD]
        self.updateEmployee[passwordField] = ''
        self.updateEmployee[repeatPasswordField] = ''
        fieldsNotChanged = set()
        for k, v in self.employeeData.items():
            if self.updateEmployee[k] == str(v):
                fieldsNotChanged.add(k)
        return fieldsNotChanged


    def _update_check(self, current_id: int):
        self.statusOk = False
        if self._is_not_self_position_update(current_id):
            if self._is_not_parking_manager_with_parking_change_position():
                self.statusOk = True


    @this_field_not_updated(UPDATE_EMPLOYEE, INDEX_POSITION)
    def _is_not_self_position_update(self, current_id: int) -> bool:
        ''' Администратор не может менять свою должность '''
        if self.id_employee == current_id:
            positionField = self.EMPLOYEE_VALUES[INDEX_POSITION]
            if self.updateEmployee.position != self.employeeData[positionField]:
                self._set_error_message(self.ERR_STATUS['error_admin_position'], 'Вы не можете изменить свою должность.')
                self.employeeData[positionField] = self.updateEmployee.position
                return False
        return True


    @this_field_not_updated(UPDATE_EMPLOYEE, INDEX_POSITION)
    def _is_not_parking_manager_with_parking_change_position(self):
        ''' Если у управляющего есть стоянка, то должность изменять нельзя '''
        if self.updateEmployee.position == Employee.PARKING_MANAGER:
            positionField = self.EMPLOYEE_VALUES[INDEX_POSITION]
            if self.employeeData[positionField] != Employee.PARKING_MANAGER:
                hasParking = hasattr(self.updateEmployee, 'carparking')
                if hasParking:
                    self._set_error_message(self.ERR_STATUS['error_manager_position'],
                        'Управляющий связан со стоянкой. Чтобы изменить его должность, установите для стоянки другого управляющего.')
                    self.employeeData[positionField] = self.updateEmployee.position
                    return False
        return True


    def _update(self):
        ed = {
            k.split(sep='_',maxsplit=1)[1]: v
            for k, v in self.employeeData.items()
            if k not in self.fieldsNotChanged
        }
        if 'name' in ed:
            self.updateEmployee.name = ed['name']
        if 'surname' in ed:
            self.updateEmployee.surname = ed['surname']
        if 'patronymic' in ed:
            self.updateEmployee.patronymic = ed['patronymic']
        if 'position' in ed:
            self.updateEmployee.position = ed['position']
        if 'phone' in ed:
            self.updateEmployee.phone = ed['phone']
        if 'password' in ed:
            self.updateEmployee.password = ed['password']
        self.updateEmployee.save()


class EmployeeDelete(ErrorsCheckAbstract):
    ERR_STATUS_DELETE = {
        'error_employee_find': 3,
        'error_parking': 4,
        'error_self': 5,
        'error_admin': 6,
    }


    def __init__(self, employeeId: int, current_id: int):
        super().__init__()
        self.ERR_STATUS.update(self.ERR_STATUS_DELETE)
        self.employeeId = employeeId
        self._check(current_id)
        if self.statusOk:
            self._delete()


    def _check(self, current_id: int):
        self.statusOk = False
        if self._id_employee_right():
            if self._is_not_self(current_id):
                if self._is_not_last_administrator():
                    if self._parking_manager_without_parking():
                        self.statusOk = True


    def _id_employee_right(self):
        try:
            self.employee = EmployeeModel.objects.get(pk=self.employeeId)
        except ObjectDoesNotExist:
            self._set_error_message(self.ERR_STATUS['error_employee_find'], 'Сотрудник не найден.')
            return False
        return True


    def _is_not_self(self, current_id: int) -> bool:
        ''' Администратор не может удалять себя '''
        if current_id == self.employee.id_employee:
            self._set_error_message(self.ERR_STATUS['error_self'],
                'Вы не можете удалить свою запись.')
            return False
        return True


    def _is_not_last_administrator(self) -> bool:
        ''' Удаляется не последний администратор,
            т.к. хотя бы один должен остаться '''
        if self.employee.position == Employee.ADMIN:
            result = call_procedure('has_administrator')
            adminCount = result[0][0] if len(result) == 1 else 0
            notLastAdmin = False
            if adminCount == 1:
                self._set_error_message(self.ERR_STATUS['error_admin'],
                    'Один администратор обязательно должен остаться.')
            else:
                notLastAdmin = True
            return notLastAdmin
        return True


    def _parking_manager_without_parking(self) -> bool:
        ''' При удалении управляющего нужно убедиться в отсутствии стоянки
            под его управлением '''
        if self.employee.position == Employee.PARKING_MANAGER:
            hasParking = hasattr(self.employee, 'carparking')
            if hasParking:
                self._set_error_message(self.ERR_STATUS['error_parking'],
                    'Управляющий связан со стоянкой. Чтобы удалить его установите для стоянки другого управляющего.')
                return False
        return True


    def _delete(self):
        self.employee.delete()


class ManagersList(ErrorsCheckAbstract):
    ERR_STATUS_FIELDS = {
        'error_manager_list': 10, # Управляющие отсутствуют
    }


    def __init__(self, freeManagers=False):
        super().__init__()
        self.ERR_STATUS.update(self.ERR_STATUS_FIELDS)
        if not freeManagers:
            self._managersList = self._all_parking_managers()
        else:
            self._managersList = self._free_parking_managers()
        self._listSize = len(self._managersList)
        if self._listSize > 0:
            self.statusOk = True
        else:
            self.statusOk = False
            self._set_error_message(self.ERR_STATUS['error_manager_list'],
                'Управляющие отсутствуют. Сначала добавьте их.')


    def _all_parking_managers(self) -> list:
        managers = EmployeeModel.objects.filter(position=Employee.PARKING_MANAGER)
        return self._managers_list(managers)


    def _free_parking_managers(self) -> list:
        managers = EmployeeModel.objects.filter(position=Employee.PARKING_MANAGER)
        freeManagers = managers.filter(carparking=None)
        return self._managers_list(freeManagers)


    def _managers_list(self, managers) -> list:
        managersList = list()
        for m in managers:
            managerDict = ManagersList.create_dict_manager_for_list(m)
            managersList.append(managerDict)
        return managersList


    def create_dict_manager_for_list(manager: EmployeeModel) -> dict:
        return {
            'id': manager.id_employee,
            'snp': manager.surname +' '+ manager.name +' '+ manager.patronymic,
        }


    def get_status(self) -> bool:
        return self.statusOk


    def get_list(self):
        return self._managersList


    def get_size(self):
        return self._listSize
