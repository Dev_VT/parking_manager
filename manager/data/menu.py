from manager.data import employee


class MenuClass:
    """Класс для создания меню на страницах"""

    STR_CONTENT = 'menu_content'
    STR_OPEN = 'menu_open'


    # Для меню указываются 'url' и 'название' пунктов
    MAIN_MENU_NOT_LOGIN = [
        ['login_page', 'Войти'],
    ]
    MAIN_MENU_LOGIN = [
        ['logout', 'Выйти'],
        ['main_page', 'Главная страница'],
    ]
    MAIN_MENU_MANAGER_LOGIN = [
        ['logout', 'Выйти'],
        ['define_parking_manager_page', 'Страница стоянки'],
    ]


    def get_menu(employeeType: str) -> list:
        if employeeType != None:
            if employeeType == employee.Employee.PARKING_MANAGER:
                return MenuClass.MAIN_MENU_MANAGER_LOGIN
            else:
                return MenuClass.MAIN_MENU_LOGIN
        else:
            return MenuClass.MAIN_MENU_NOT_LOGIN
