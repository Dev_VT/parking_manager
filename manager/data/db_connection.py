from django.db import connection


# Вызов процедуры из базы данных
def call_procedure(procedure_name: str) -> tuple:
    result = tuple()
    with connection.cursor() as cursor:
        cursor.callproc(procedure_name)
        result = cursor.fetchall()
    return result


def call_procedure_with_args(procedure_name: str, *args) -> tuple:
    result = tuple()
    with connection.cursor() as cursor:
        cursor.callproc(procedure_name, args)
        result = cursor.fetchall()
    return result
