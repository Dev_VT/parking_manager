from xml.etree import ElementTree
from manager.data import employee
from manager.models import CarBrand
from manager.app_tests.suppliers import car_supplier, employee_supplier, parking_supplier


MAIN_ADMINISTRATOR = employee_supplier.TEST_EMPLOYEES[0]


def add_main_administrator() -> int:
    adminList = list(MAIN_ADMINISTRATOR)
    adminList.append(adminList[employee.INDEX_PASSWORD]) # продублировать пароль
    adminDict = {
        employee.EmployeeAddNew.EMPLOYEE_FIELDS[i]: adminList[i]
        for i in range(len(adminList))
    }
    e = employee.EmployeeAddNew(adminDict)
    return e.get_employee_id()


def first_login():
    tcmSupplier = car_supplier.TestCarModelSupplier()
    tcmSupplier.add_car_models_from_file()
    add_test_data(tcmSupplier)


def add_test_data(tcmSupplier: 'TestCarModelSupplier'):
    teSupplier = employee_supplier.TestEmployeeSupplier()
    teSupplier.add_employees()
    tpSupplier = parking_supplier.TestParkingSupplier(teSupplier)
    tpSupplier.add_parking_lots()
    tcSupplier = car_supplier.TestCarSupplier(tpSupplier, tcmSupplier)
    tcSupplier.add_cars()
