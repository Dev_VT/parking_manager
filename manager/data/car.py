from django.core.exceptions import ObjectDoesNotExist
from manager.data.db_connection import *
from manager.data.errors_check import ErrorsCheckAbstract, ErrorsCheckFieldsAbstract
from manager.data.errors_check import this_field_not_updated, UPDATE_CAR
from manager.data.employee import EmployeesList
from manager.data.parking import ParkingList
from manager.data import car_model
from manager import models


INDEX_GOV_CAR_NUMBER = 0
INDEX_ID_NUMBER = 1
INDEX_MODEL = 2
INDEX_COLOR = 3
INDEX_MILEAGE = 4
INDEX_OWNER_NAME = 5
INDEX_OWNER_SURNAME = 6
INDEX_OWNER_PATRONYMIC = 7
INDEX_PHONE = 8


class Car:
    CAR_COLOURS = {
        0: 'Другой',
        1: 'Черный',
        2: 'Серый',
        3: 'Белый',
        4: 'Синий',
        5: 'Красный',
        6: 'Коричневый',
        7: 'Зеленый',
        8: 'Желтый',
        9: 'Оранжевый',
    }

    def get_car_values(carId: int):
        c = models.Car.objects.get(pk=carId)
        dataValues = CarCheckAbstract.CAR_VALUES
        carDict = {
            dataValues[INDEX_GOV_CAR_NUMBER]: c.gov_car_number,
            dataValues[INDEX_ID_NUMBER]: c.id_number.id_number,
            dataValues[INDEX_MODEL]: c.id_model.id_model,
            dataValues[INDEX_COLOR]: c.color,
            dataValues[INDEX_MILEAGE]: c.mileage,
            dataValues[INDEX_OWNER_NAME]: c.owner_name,
            dataValues[INDEX_OWNER_SURNAME]: c.owner_surname,
            dataValues[INDEX_OWNER_PATRONYMIC]: c.owner_patronymic,
            dataValues[INDEX_PHONE]: c.phone,
        }
        return carDict


    def get_car_values_in_strings(carId: int) -> dict:
        carDict = Car.get_car_values(carId)
        return {k: str(v) for k, v in carDict.items()}


    def get_car_brands() -> dict:
        carBrandSet = set()
        carBrandList = (list(), list())
        ml = call_procedure('model_list')
        for carModel in ml:
            if carModel[0] not in carBrandSet:
                carBrandSet.add(carModel[0])
                carBrandList[0].append((carModel[0], carModel[1]))
            carModelTitle = carModel[1] + ' ' + carModel[3]
            carBrandList[1].append((carModel[2], carModelTitle))
        return carBrandList


class CarList:
    def __init__(self, parkingId: int, snpUnion: bool=True):
        carTuple = self._get_car_list(parkingId, snpUnion)
        self._carList = self._list_transformation(carTuple, snpUnion)
        self._listSize = len(self._carList)


    def _get_car_list(self, parkingId: int, snpUnion: bool) -> tuple:
        cl = call_procedure_with_args('car_list', parkingId, snpUnion)
        return cl


    def _list_transformation(self, carTuple: tuple, snpUnion: bool):
        updatedCarList = list()
        for c in carTuple:
            c = list(c)
            CarList.set_color(c)
            phoneIndex = 7 if snpUnion else 9
            if c[phoneIndex] != '':
                c[phoneIndex] = EmployeesList.phone_transformation(c[phoneIndex])
            updatedCarList.append(c)
        return updatedCarList


    def set_color(carWithoutColor: list):
        carWithoutColor[4] = Car.CAR_COLOURS[carWithoutColor[4]]


    def get_list(self):
        return self._carList


    def get_size(self):
        return self._listSize


class CarCheckAbstract(ErrorsCheckFieldsAbstract):
    CAR_FIELDS = (
        'gov_car_number_field',
        'parking_field',
        'id_model_field',
        'color_field',
        'mileage_field',
        'owner_name_field',
        'owner_surname_field',
        'owner_patronymic_field',
        'phone_field',
    )
    CAR_VALUES = (
        'car_gov_car_number',
        'car_id_number',
        'car_id_model',
        'car_color',
        'car_mileage',
        'car_owner_name',
        'car_owner_surname',
        'car_owner_patronymic',
        'car_phone',
    )
    ERR_STATUS_CHECK = {
        'error_not_int': 3,
        'error_number_used': 4,
        'error_color': 5,
        'error_gov_number': 6,
        'error_gov_number_used': 7,
        'error_phone': 8,
        'error_phone_used': 9,
        'error_parking_find': 10,
        'error_model_find': 11,
    }


    def __init__(self, carId: int=-1):
        super().__init__()
        self.ERR_STATUS.update(self.ERR_STATUS_CHECK)
        self.newModel = None
        self.selectedParking = None
        self.selectedModel = None
        self._variablesForUpdate(carId)


    def _variablesForUpdate(self, carId: int):
        self.updateCar = None
        self.fieldsNotChanged = None


    def read_car_data(self, request_POST: dict):
        self.carData = {
            self.CAR_VALUES[
                self.CAR_FIELDS.index(k)]: v
            for k, v in request_POST.items()
            if k in self.CAR_FIELDS
        }
        if car_model.ModelAddNew.need_add_new_model(request_POST):
            self.newModel = car_model.ModelAddNew(request_POST, checkAndReadNow=False)
            self.newModel.read_brand_data(request_POST)


    def check_fields(self):
        self.statusOk = False
        if self._check_new_model():
            if self._has_not_empty_fields():
                if self._fields_be_int():
                    if self._is_right_color():
                        if self._goverment_number_verification():
                            if self._goverment_number_unique():
                                if self._phone_verification():
                                    if self._change_id_to_parking():
                                        if self._change_id_to_model():
                                            self.statusOk = True


    def _check_new_model(self):
        if self.newModel != None:
            self.newModel.check_fields()
            if not self.newModel.get_status():
                self._set_error_message(self.newModel.err_status, self.newModel.get_msg())
                return False
        return True


    def _has_not_empty_fields(self) -> bool:
        NOT_EMPTY_FIELDS = (0,1,2,3,4,5,6) if self.newModel == None else (0,1,3,4,5,6)
        try:
            for i in NOT_EMPTY_FIELDS:
                if self.carData[self.CAR_VALUES[i]] == '':
                    self._set_error_message(self.ERR_STATUS['error_field'], 'Не все поля заполнены!')
                    return False
        except KeyError:
            self._set_error_message(self.ERR_STATUS['error_field'], 'Данные не были переданы.')
            return False
        return True


    def _fields_be_int(self):
        INT_FIELDS = (1,2,3,4) if self.newModel == None else (1,3,4)
        try:
            for i in INT_FIELDS:
                k = self.CAR_VALUES[i]
                self.carData[k] = int(self.carData[k])
                if self.carData[k] < 0:
                    raise ValueError
        except ValueError:
            self._set_error_message(self.ERR_STATUS['error_not_int'], 'Номер введен неверно.')
            return False
        return True


    @this_field_not_updated(UPDATE_CAR, INDEX_COLOR)
    def _is_right_color(self):
        colorNumber = self.carData[self.CAR_VALUES[INDEX_COLOR]]
        if not colorNumber in Car.CAR_COLOURS:
            self._set_error_message(self.ERR_STATUS['error_color'],
                "Неверное значение цвета автомобиля.")
            return False
        return True


    @this_field_not_updated(UPDATE_CAR, INDEX_GOV_CAR_NUMBER)
    def _goverment_number_verification(self):
        govNumberKey = self.CAR_VALUES[INDEX_GOV_CAR_NUMBER]
        govermentNumber = self.carData[govNumberKey]
        if len(govermentNumber) >= 8:
            chars = govermentNumber[0] + govermentNumber[4:5]
            numbers = govermentNumber[1:3] + govermentNumber[6:8]
            if chars.isalpha() and numbers.isnumeric():
                self.carData[govNumberKey] = govermentNumber.lower()
                return True
        self._set_error_message(self.ERR_STATUS['error_gov_number'],
            "Номер автомобиля введен неверно. Пример: 'т123оп138' или 'т123оп38'.")
        return False


    @this_field_not_updated(UPDATE_CAR, INDEX_GOV_CAR_NUMBER)
    def _goverment_number_unique(self):
        govNumberKey = self.CAR_VALUES[INDEX_GOV_CAR_NUMBER]
        govermentNumber = self.carData[govNumberKey]
        carList = models.Car.objects.filter(gov_car_number=govermentNumber)
        if carList.count() > 0:
            self._set_error_message(self.ERR_STATUS['error_gov_number_used'],
                'Данный государственный номер уже используется!')
            return False
        return True


    @this_field_not_updated(UPDATE_CAR, INDEX_PHONE)
    def _phone_verification(self):
        phone = self.carData[self.CAR_VALUES[INDEX_PHONE]]
        if phone == '':
            return True
        if phone[0] == '+':
            phone = phone[1:len(phone)]
        phoneLenght = len(phone)
        if phoneLenght == 11 and phone.isdigit():
            self.carData[self.CAR_VALUES[INDEX_PHONE]] = phone
            return True
        self._set_error_message(self.ERR_STATUS['error_phone'], 'Номер телефона указан неверно.')
        return False


    @this_field_not_updated(UPDATE_CAR, INDEX_ID_NUMBER)
    def _change_id_to_parking(self):
        try:
            idParking = self.carData[self.CAR_VALUES[INDEX_ID_NUMBER]]
            self.selectedParking = models.CarParking.objects.get(pk=idParking)
        except ObjectDoesNotExist:
            self._set_error_message(self.ERR_STATUS['error_parking_find'], 'Стоянка не найдена.')
            return False
        return True


    def _change_id_to_model(self):
        if self.newModel == None:
            try:
                idModel = self.carData[self.CAR_VALUES[INDEX_MODEL]]
                self.selectedModel = models.CarModel.objects.get(pk=idModel)
            except ObjectDoesNotExist:
                self._set_error_message(self.ERR_STATUS['error_model_find'], 'Модель не найдена.')
                return False
        else:
            self.newModel.add_new_model()
            self.selectedModel = self.newModel.get_added_model()
        return True


    def get_car_data(self) -> dict:
        if self.newModel != None:
            self.carData.update(self.newModel.get_model_data())
        return self.carData


    def get_parking_id(self) -> int:
        parkingId = self.carData[self.CAR_VALUES[INDEX_ID_NUMBER]]
        if not isinstance(parkingId, int):
            parkingId = parkingId.id_number
        return parkingId


class CarAddNew(CarCheckAbstract):
    def __init__(self, request_POST: dict, parkingId: int):
        ''' Автомобиль можно добавить только к конкретной парковке,
            выбирать стоянку при создании авто нельзя '''
        super().__init__()
        self.read_car_data(request_POST)
        self.carData[self.CAR_VALUES[INDEX_ID_NUMBER]] = parkingId
        self.check_fields()
        if self.statusOk:
            self._add()


    def _add(self):
        self.carData[self.CAR_VALUES[INDEX_MODEL]] = self.selectedModel
        self.carData[self.CAR_VALUES[INDEX_ID_NUMBER]] = self.selectedParking
        cd = {k.split(sep='_',maxsplit=1)[1]: v for k, v in self.carData.items()}
        self.newCar = models.Car(**cd)
        self.newCar.save()


    def get_car_model(self):
        return self.newCar


class CarUpdate(CarCheckAbstract):
    def __init__(self, request_POST: dict, carId: int=-1):
        super().__init__(carId)
        self.read_car_data(request_POST)
        self.updateCar = Car.get_car_values_in_strings(carId)
        self._set_parking_id(self.updateCar)
        self.fieldsNotChanged = self._get_fields_not_changed()
        self.check_fields()
        if self.statusOk:
            self.updateCar = models.Car.objects.get(pk=carId)
            self._update()


    def _set_parking_id(self, updateCar: dict):
        # поле выбора стоянки у управляющего заблокировано,
        # поэтому id не может быть передано
        parkingIdKey = self.CAR_VALUES[INDEX_ID_NUMBER]
        if parkingIdKey not in self.carData:
            self.carData[parkingIdKey] = updateCar[parkingIdKey]


    def _get_fields_not_changed(self) -> set:
        ''' Значения каких полей не были изменены '''
        fieldsNotChanged = set()
        for k, v in self.carData.items():
            if self.updateCar[k] == str(v):
                fieldsNotChanged.add(k)
        return fieldsNotChanged


    def _update(self):
        cd = {
            k.split(sep='_',maxsplit=1)[1]: v
            for k, v in self.carData.items()
            if k not in self.fieldsNotChanged
        }
        if 'gov_car_number' in cd:
            self.updateCar.gov_car_number = cd['gov_car_number']
        if 'id_number' in cd:
            self.updateCar.id_number = self.selectedParking
        if 'id_model' in cd or self.newModel != None:
            self.updateCar.id_model = self.selectedModel
        if 'color' in cd:
            self.updateCar.color = cd['color']
        if 'mileage' in cd:
            self.updateCar.mileage = cd['mileage']
        if 'owner_name' in cd:
            self.updateCar.owner_name = cd['owner_name']
        if 'owner_surname' in cd:
            self.updateCar.owner_surname = cd['owner_surname']
        if 'owner_patronymic' in cd:
            self.updateCar.owner_patronymic = cd['owner_patronymic']
        if 'phone' in cd:
            self.updateCar.phone = cd['phone']
        self.updateCar.save()


class CarDelete(ErrorsCheckAbstract):
    ERR_STATUS_DELETE = {
        'error_car_find': 10,
    }


    def __init__(self, carId: int):
        super().__init__()
        self.ERR_STATUS.update(self.ERR_STATUS_DELETE)
        self.carId = carId
        self.parkingId = None
        self._check()
        if self.statusOk:
            self._delete()


    def _check(self):
        self.statusOk = False
        if self._id_car_right():
            self.statusOk = True


    def _id_car_right(self):
        try:
            self.car = models.Car.objects.get(pk=self.carId)
            self.parkingId = self.car.id_number.id_number
        except ObjectDoesNotExist:
            self._set_error_message(self.ERR_STATUS['error_car_find'], 'Автомобиль не найден.')
            return False
        return True


    def _delete(self):
        self.car.delete()


    def get_parking_id(self):
        return self.parkingId


class SearchCar(ErrorsCheckFieldsAbstract):
    SEARCH_FIELDS = (
        'search_field',
    )
    SEARCH_VALUES = (
        'car_search',
    )
    ERR_STATUS_SEARCH = {
        'error_gov_number': 10,
    }

    def __init__(self, request_GET: str):
        super().__init__()
        self.ERR_STATUS.update(self.ERR_STATUS_SEARCH)
        self.read_search_data(request_GET)
        self.check_fields()
        if self.statusOk:
            self._search()


    def read_search_data(self, request_GET: dict):
        self.searchData = {
            self.SEARCH_VALUES[
                self.SEARCH_FIELDS.index(k)]: v
            for k, v in request_GET.items()
            if k in self.SEARCH_FIELDS
        }


    def check_fields(self):
        self.statusOk = False
        if self._has_not_empty_fields():
            if self._goverment_number_verification():
                self.statusOk = True


    def _has_not_empty_fields(self) -> bool:
        NOT_EMPTY_FIELDS = (0,)
        try:
            for i in NOT_EMPTY_FIELDS:
                if self.searchData[self.SEARCH_VALUES[i]] == '':
                    self._set_error_message(self.ERR_STATUS['error_field'], 'Не все поля заполнены!')
                    return False
        except KeyError:
            self._set_error_message(self.ERR_STATUS['error_field'], 'Данные не были переданы.')
            return False
        return True


    def _goverment_number_verification(self):
        govNumberKey = self.SEARCH_VALUES[0]
        govermentNumber = self.searchData[govNumberKey]
        if len(govermentNumber) > 9:
            self._set_error_message(self.ERR_STATUS['error_gov_number'],
                "Длина номера не должна быть более 9 символов.")
            return False
        return True


    def _search(self):
        govNumberKey = self.SEARCH_VALUES[0]
        govermentNumber = self.searchData[govNumberKey]
        carTuple = call_procedure_with_args('search_car', govermentNumber)
        self.carList = self._list_transformation(carTuple)
        self.carCount = len(self.carList)


    def _list_transformation(self, carTuple: tuple):
        updatedCarList = list()
        for c in carTuple:
            c = list(c)
            CarList.set_color(c)
            SearchCar.parking_transformation(c)
            if c[7] != '':
                c[7] = EmployeesList.phone_transformation(c[7])
            updatedCarList.append(c)
        return updatedCarList


    def parking_transformation(carOwner: list):
        carOwner[8] = '№' + str(carOwner[8]) + ' ' + carOwner[9]
        carOwner.pop(9)


    def get_search_data(self):
        return self.searchData


    def get_count(self):
        return self.carCount


    def get_list(self):
        return self.carList
