from reportlab.pdfgen import canvas
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib.units import cm, mm
import datetime
import io
from manager.data import parking
from manager.data.car import CarList


NORM_FONT = 'Times-New-Roman'
BOLD_FONT = 'Times-New-Roman-Bold'


class DocumentAbstract:
    MARGIN_LEFT = 25 * mm
    MARGIN_RIGHT = 15 * mm
    MARGIN_BOTTOM = 2 * cm
    MARGIN_PARAGRAPH = 1 * cm
    MARGIN_PAGE_NUMBER_BOTTOM = 6


    def __init__(self):
        self.createDate = self._add_create_date()
        self.pageRight = 21*cm - self.MARGIN_LEFT - self.MARGIN_RIGHT
        self.pageCenter = self.pageRight/2
        self._add_fonts_with_cyrillic()


    def _add_create_date(self):
        today = datetime.date.today()
        return today.strftime("%d-%m-%Y")


    def _add_fonts_with_cyrillic(self):
        MyFontObject = TTFont(NORM_FONT, 'manager/files/fonts/times-new-roman.ttf')
        pdfmetrics.registerFont(MyFontObject)
        MyFontObject = TTFont(BOLD_FONT, 'manager/files/fonts/times-new-roman-bold.ttf')
        pdfmetrics.registerFont(MyFontObject)


    def create_pdf(self) -> bool:
        self.buffer = io.BytesIO()
        self.doc = canvas.Canvas(self.buffer)
        self.add_content()
        self.doc.save()
        return True


    def add_content(self):
        pass


    def new_page(self) -> canvas.Canvas:
        self.doc.translate(self.MARGIN_LEFT, self.MARGIN_BOTTOM)
        self.pagePosition = 255
        return self.doc


    def close_page(self):
        self._add_page_number()
        self.doc.showPage()


    def _add_page_number(self):
        pageNumber = str(self.doc.getPageNumber())
        self.doc.setFont(NORM_FONT, 10)
        self.doc.drawRightString(self.pageRight, 0, pageNumber + " страница")


    def add_gray_line(self):
        self.doc.setStrokeColorRGB(0.5, 0.5, 0.5)
        self._add_line()


    def add_blue_line(self):
        self.doc.setStrokeColorRGB(0.7, 0.8, 1)
        self._add_line()


    def _add_line(self):
        self.doc.setLineWidth(2)
        linePosition = self._line_position()
        lineLenght = self.pageCenter * 2
        self.doc.line(0, linePosition, lineLenght, linePosition)


    def _line_position(self):
        self.pagePosition -= 4
        return self.pagePosition * mm


    def string_position(self, minusMM: int=8):
        self.pagePosition -= minusMM
        return self.pagePosition * mm


    def get_pdf(self) -> io.BytesIO:
        self.buffer.seek(0)
        return self.buffer


    def get_file_name(self) -> str:
        pass


class DocumentParkingList(DocumentAbstract):
    PARKING_HEIGHT = 34


    # override
    def add_content(self):
        class ParkingListOrderTitle(parking.ParkingList):
            PROCEDURE_NAME = 'document_parking_list'
        parkingListObject = ParkingListOrderTitle()
        parkingList = parkingListObject.get_list()
        self.doc.setTitle('Список стоянок на ' + self.createDate)
        self.new_page()
        self._add_title()
        self._add_parking_lots(parkingList)
        self.close_page()


    def _add_title(self):
        self.doc.setFont(BOLD_FONT, 20)
        self.doc.drawCentredString(self.pageCenter, self.pagePosition*mm, "Список стоянок")
        self.doc.setFont(NORM_FONT, 12)
        self.doc.drawString(self.MARGIN_PARAGRAPH, self.string_position(),
            "Дата создания документа " + self.createDate + '.')
        self.add_gray_line()


    def _add_parking_lots(self, parkingList: tuple):
        for p in parkingList:
            if not self._could_be_add_parking_on_page():
                self.close_page()
                self.doc = self.new_page()
            self._add_parking(p)


    def _could_be_add_parking_on_page(self) -> bool:
        needSpace = self.PARKING_HEIGHT + self.MARGIN_PAGE_NUMBER_BOTTOM
        return needSpace <= self.pagePosition


    def _add_parking(self, parking: tuple):
        self.doc.setFont(NORM_FONT, 14)
        self.doc.drawString(self.MARGIN_PARAGRAPH, self.string_position(),
            "Стоянка \'" + parking[1] + "\' №" + str(parking[2]) + ".")
        self.doc.setFont(NORM_FONT, 12)
        self.doc.drawString(0, self.string_position(),
            "Адрес: " + parking[3] + " район, улица " + parking[4] + ".")
        self.doc.drawString(0, self.string_position(7),
            "Управляющий: " + parking[5] + ".")
        self.doc.drawRightString(self.pageRight, self.string_position(7),
            "Телефон: " + parking[6])
        self.add_blue_line()


    # override
    def get_file_name(self) -> str:
        return 'parking_list_' + self.createDate + '.pdf'


class DocumentCarTableAbstract(DocumentAbstract):
    TABLE_MARGIN_TOP = 4
    TABLE_MARGIN_BOTTOM = 4
    TABLE_HEADERS_HEIGHT = 7
    TABLE_CAR_ROWS_HEIGHT = 12
    COLUMN_LENGHTS_MM = (8, 20, 30, 23, 16, 43, 30)
    MARGIN_COLUMN = 2*mm
    COLUMN_MARGINS = list() # вычисляется в методе по COLUMN_LENGHTS_MM


    def __init__(self):
        super().__init__()
        self._set_table_columns()


    def _set_table_columns(self):
        self.COLUMN_MARGINS = [0,]
        for COLUMN_LENGHT in self.COLUMN_LENGHTS_MM:
            nextMargin = self.COLUMN_MARGINS[-1] + COLUMN_LENGHT
            self.COLUMN_MARGINS.append(nextMargin)
        self.COLUMN_MARGINS = [margin*mm for margin in self.COLUMN_MARGINS]


    def set_parking_info_for_table(self, parkingTitle: str, parkingNumber: int):
        self.title = parkingTitle
        self.parkingNumber = str(parkingNumber)


    def add_cars(self, carList: tuple):
        carCount = len(carList)
        carCountCouldBeAddPage = 0
        for carIndex in range(carCount):
            if carCountCouldBeAddPage == 0:
                if carIndex > 0: # если авто уже добавляются, нужна новая страница
                    self.close_page()
                    self.doc = self.new_page()
                    self._continue_table_on_next_page()
                carCountCouldBeAddPage = self._page_car_count(carCount)
                self._draw_table(carCountCouldBeAddPage)
            self._add_car(carList[carIndex], carIndex)
            carCount -= 1
            carCountCouldBeAddPage -= 1
        self.pagePosition -= self.TABLE_MARGIN_BOTTOM


    def _page_car_count(self, carCount: int) -> int:
        pageMaxCarCount = self._page_car_count_could_be_add()
        return pageMaxCarCount if carCount > pageMaxCarCount else carCount


    def _page_car_count_could_be_add(self) -> int:
        freeSpace = self.pagePosition-(self.TABLE_MARGIN_TOP+self.TABLE_HEADERS_HEIGHT+self.MARGIN_PAGE_NUMBER_BOTTOM)
        carCountPage = int(freeSpace / self.TABLE_CAR_ROWS_HEIGHT)
        return carCountPage


    def _draw_table(self, carCount):
        self.pagePosition -= self.TABLE_MARGIN_TOP
        rowList = list()
        gridPosition = self.pagePosition
        for i in range(carCount+2):
            rowList.append(gridPosition*mm)
            gridPosition -= self.TABLE_CAR_ROWS_HEIGHT if i > 0 else self.TABLE_HEADERS_HEIGHT
        self.doc.setStrokeColorRGB(0.5, 0.5, 0.5)
        self.doc.setLineWidth(1)
        self.doc.grid(self.COLUMN_MARGINS, rowList)
        self._add_table_headers()


    def _add_table_headers(self):
        self.doc.setFont(BOLD_FONT, 10)
        titleRowPosition = self.string_position(5)
        margins = [m + self.MARGIN_COLUMN for m in self.COLUMN_MARGINS]
        headers = ('№','Госномер','Марка и модель','Цвет','Пробег','Владелец','Телефон')
        for i in range(len(headers)):
            self.doc.drawString(margins[i], titleRowPosition, headers[i])


    def _add_car(self, car: tuple, carIndex: int):
        self.doc.setFont(NORM_FONT, 10)
        carRowPosition = self.string_position(7)
        margins = [m + self.MARGIN_COLUMN for m in self.COLUMN_MARGINS]
        values = (str(carIndex+1), car[1], car[2], car[4], str(car[5]),
            car[6] + ' ' + car[7], car[9])
        for i in range(len(values)):
            self.doc.drawString(margins[i], carRowPosition, values[i])
        carRowPosition2 = self.string_position(5)
        self.doc.drawString(margins[2], carRowPosition2, car[3])
        self.doc.drawString(margins[5], carRowPosition2, car[8])


    def _continue_table_on_next_page(self):
        self.doc.setFont(NORM_FONT, 10)
        self.doc.drawRightString(self.pageRight, self.pagePosition*mm,
            "Продолжение таблицы, стоянка \'" + self.title + "\' №" + self.parkingNumber)
        self.add_blue_line()


class DocumentCarList(DocumentCarTableAbstract):
    def __init__(self, parkingId: int):
        super().__init__()
        self._set_parking_id(parkingId)


    def _set_parking_id(self, parkingId: int):
        self.parkingId = parkingId
        parkingData = parking.Parking.get_parking_values(parkingId)
        dataValues = parking.ParkingCheckAbstract.PARKING_VALUES
        self.set_parking_info_for_table(
            parkingData[dataValues[parking.INDEX_TITLE]],
            parkingData[dataValues[parking.INDEX_PARKING_NUMBER]])
        self.district = parkingData[dataValues[parking.INDEX_DISTRICT]]
        self.street = parkingData[dataValues[parking.INDEX_STREET]]


    # override
    def add_content(self):
        clObject = CarList(self.parkingId, snpUnion=False)
        carList = clObject.get_list()
        self.doc.setTitle('Список автомобилей стоянки №' + self.parkingNumber + ' на ' + self.createDate)
        self.new_page()
        self._add_title()
        self.add_cars(carList)
        self.close_page()


    def _add_title(self):
        self.doc.setFont(BOLD_FONT, 20)
        self.doc.drawCentredString(self.pageCenter, self.pagePosition*mm,
            "Стоянка \'" + self.title + "\' №" + self.parkingNumber)
        self.doc.setFont(NORM_FONT, 12)
        self.doc.drawString(self.MARGIN_PARAGRAPH, self.string_position(),
            "Адрес: " + self.district + " район, улица " + self.street + ".")
        self.doc.drawString(self.MARGIN_PARAGRAPH, self.string_position(),
            "Дата создания документа " + self.createDate + '.')
        self.add_blue_line()

    # override
    def get_file_name(self) -> str:
        return 'car_list_' + self.parkingNumber + '_' + self.createDate + '.pdf'


class DocumentAllCarList(DocumentCarTableAbstract):
    PARKING_HEADER_HEIGHT = 28


    # override
    def add_content(self):
        class ParkingListWithCarCount(parking.ParkingList):
            PROCEDURE_NAME = 'document_parking_list_with_car_count'
        parkingListObject = ParkingListWithCarCount()
        parkingList = parkingListObject.get_list()
        self.doc.setTitle('Список всех автомобилей на ' + self.createDate)
        self.new_page()
        self._add_title()
        self._add_parking_lots(parkingList)
        self.close_page()


    def _add_title(self):
        self.doc.setFont(BOLD_FONT, 20)
        self.doc.drawCentredString(self.pageCenter, self.pagePosition*mm,
            "Список автомобилей на всех стоянках")
        self.doc.setFont(NORM_FONT, 12)
        self.doc.drawString(self.MARGIN_PARAGRAPH, self.string_position(),
            "Дата создания документа " + self.createDate + '.')


    def _add_parking_lots(self, parkingList: tuple):
        for p in parkingList:
            if not self._could_be_add_parking_on_page():
                self.close_page()
                self.doc = self.new_page()
            self._add_parking(p)
            self.set_parking_info_for_table(p[1], p[2])
            carListObject = CarList(p[0], snpUnion=False)
            carList = carListObject.get_list()
            self.add_cars(carList)


    def _could_be_add_parking_on_page(self) -> bool:
        needSpace = (
            self.PARKING_HEADER_HEIGHT +
            self.TABLE_MARGIN_TOP +
            self.TABLE_HEADERS_HEIGHT +
            self.TABLE_CAR_ROWS_HEIGHT)
        return needSpace <= self.pagePosition


    def _add_parking(self, parking: tuple):
        self.add_blue_line()
        self.doc.setFont(NORM_FONT, 12)
        self.doc.drawString(self.MARGIN_PARAGRAPH, self.string_position(),
            "Стоянка \'" + parking[1] + "\' №" + str(parking[2]) + ".")
        self.doc.setFont(NORM_FONT, 10)
        stringPosition = self.string_position()
        self.doc.drawString(0, stringPosition,
            "Адрес: " + parking[3] + " район, улица " + parking[4] + ".")
        self.doc.drawRightString(self.pageRight, stringPosition,
            "Телефон: " + parking[6])
        stringPosition = self.string_position()
        self.doc.drawString(0, stringPosition,
            "Управляющий: " + parking[5] + ".")
        self.doc.drawRightString(self.pageRight, stringPosition,
            "Кол-во машин: " + str(parking[7]))

    # override
    def get_file_name(self) -> str:
        return 'all_car_list_' + self.createDate + '.pdf'
