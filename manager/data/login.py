from django.http import HttpResponseRedirect
from django.urls import reverse
from manager.data.db_connection import call_procedure_with_args, call_procedure
from functools import wraps
from manager.data.errors_check import ErrorsCheckFieldsAbstract
from manager.data import employee, first_login


class Login(ErrorsCheckFieldsAbstract):
    ERR_STATUS_LOGIN = {
        'error_password': 3,
    }


    def __init__(self, password):
        super().__init__()
        self.ERR_STATUS.update(self.ERR_STATUS_LOGIN)
        self.is_login = False
        if self._is_not_empty_password(password):
            if self._password_verification(password):
                if self._find_employee(password):
                    self.is_login = True


    def _is_not_empty_password(self, password) -> bool:
        if password == '':
            self._set_error_message(self.ERR_STATUS['error_field'], 'Вы не ввели пароль')
            return False
        return True


    def _password_verification(self, password) -> bool:
        password_length = len(password)
        if password_length < 6 or password_length > 12:
            self._set_error_message(self.ERR_STATUS['error_password'], 'Длина пароля должна быть от 6 до 12 символов.')
            return False
        return True


    def _find_employee(self, password: str) -> dict:
        self.employeeLoginId = None
        e = call_procedure_with_args('login_employee', password)
        if len(e) > 0:
            self.employeeLoginId = e[0][0]
            return True
        elif self._is_first_login(password):
            return True
        else:
            self._set_error_message(self.ERR_STATUS['error_password'], 'Неверный пароль')
            return False


    def _is_first_login(self, password: str) -> bool:
        mainAdministrator = first_login.MAIN_ADMINISTRATOR
        if password == mainAdministrator[employee.INDEX_PASSWORD]:
            if self._app_has_not_administrator():
                self.employeeLoginId = first_login.add_main_administrator()
                first_login.first_login()
                return True
        return False


    def _app_has_not_administrator(self):
        result = call_procedure('has_administrator')
        return False if len(result) == 1 and result[0][0] > 0 else True


    def is_login_correct(self) -> bool:
        return self.is_login


    def get_login_id(self) -> dict:
        return self.employeeLoginId


def is_login(func):
    ''' Декоратор для проверки сотрудника на странице login,
        не вошёл ли он уже в сервис '''
    @wraps(func)
    def wrapper(*args, **kwargs):
        request = args[0]
        position = None
        try:
            employeeLogin = request.session['employee']
            position = employeeLogin['employee_position']
        except KeyError:
            return func(*args, **kwargs)

        if position == employee.Employee.ADMIN or position == employee.Employee.SECRETARY:
            return HttpResponseRedirect(reverse('main_page'))
        elif position == employee.Employee.PARKING_MANAGER:
            return HttpResponseRedirect(reverse('define_parking_manager_page'))
    return wrapper


def restriction_check(*positions: str):
    def employee_check(func):
        ''' Декоратор определяет, какие типы сотрудников
            не должны получить доступ к странице, к которой от добавлен '''
        @wraps(func)
        def wrapper(*args, **kwargs):
            request = args[0]
            position = None
            try:
                employeeLogin = request.session['employee']
                position = employeeLogin['employee_position']
            except KeyError:
                return HttpResponseRedirect(reverse('login_page'))

            if position in positions:
                mainUrl = 'main_page'
                if position == employee.Employee.PARKING_MANAGER:
                    mainUrl = 'define_parking_manager_page'
                return HttpResponseRedirect(reverse(mainUrl))
            return func(*args, **kwargs)
        return wrapper
    return employee_check
