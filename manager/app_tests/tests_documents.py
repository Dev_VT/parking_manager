from django.test import TestCase
from manager.app_tests.suppliers.parking_supplier import TestParkingSupplier
from manager.app_tests.suppliers.car_supplier import TestCarSupplier
from manager.data import documents


class CreateDocumentsTest(TestCase):
    def setUp(self):
        tpSupplier = TestParkingSupplier()
        tpSupplier.add_parking_lots(2)
        self.parkingId = tpSupplier.get_random_parking_id()
        tcSupplier = TestCarSupplier(tpSupplier)


    def test_document_parking_list(self):
        document = documents.DocumentParkingList()
        status = document.create_pdf()
        self.assertTrue(status)


    def test_document_car_list(self):
        document = documents.DocumentCarList(self.parkingId)
        status = document.create_pdf()
        self.assertTrue(status)


    def test_document_all_car_list(self):
        document = documents.DocumentAllCarList()
        status = document.create_pdf()
        self.assertTrue(status)
