from manager.app_tests.suppliers.employee_supplier import TestEmployeeSupplier
from manager.data import parking
from random import randrange


TEST_PARKINGS = (
    ('10', 'У центрального рынка', 'Правобережный', 'Тимирязева', '1'),
    ('22', 'БЦ "Троиций"', 'Правобережный', '5-й Армии', '1'),
    ('', 'ЖК "Флагман"', 'Свердловский', 'Ломоносова', '1'),
)


class TestParkingSupplier():
    def __init__(self, teSupplier: TestEmployeeSupplier=None):
        self.addedParkingDict = dict()
        self.parkingIdList = list()
        self.teSupplier = teSupplier


    def add_parking_lots(self, count: int=-1):
        count = len(TEST_PARKINGS) if count <= -1 or count > len(TEST_PARKINGS) else count
        parkingManagerList = self._add_parking_managers(count)
        parkingManagerCount = len(parkingManagerList)
        count = count if count <= parkingManagerCount else parkingManagerCount
        for i in range(count):
            self._add_parking(TEST_PARKINGS[i], parkingManagerList[i])


    def add_next_parking(self):
        parkingCount = len(self.parkingIdList)
        addParkingIndex = parkingCount
        parkingManager = self.teSupplier.get_parking_manager_id(addParkingIndex)
        self._add_parking(TEST_PARKINGS[addParkingIndex], parkingManager)


    def _add_parking_managers(self, count):
        if self.teSupplier == None:
            self.teSupplier = TestEmployeeSupplier()
            self.teSupplier.add_parking_managers(count)
        return self.teSupplier.get_parking_manager_ids_list()


    def _add_parking(self, testParking: tuple, idParkingManager: int):
        dictPOST = ParkingDictPOST.create_dict_POST(testParking, idEmployee=idParkingManager)
        p = parking.ParkingAddNew(dictPOST)
        if p.get_status():
            newParkingModel = p.get_parking_model()
            self._add_parking_to_list(newParkingModel)


    def _add_parking_to_list(self, newParkingModel):
        newParking = [
            newParkingModel.parking_number,
            newParkingModel.title,
            newParkingModel.district,
            newParkingModel.street,
            newParkingModel.id_employee.id_employee,
        ]
        self.addedParkingDict[newParkingModel.id_number] = newParking
        self.parkingIdList.append(newParkingModel.id_number)


    def get_parking_id(self, index: int=0):
        return self.parkingIdList[index]


    def get_random_parking_id(self):
        return self.parkingIdList[randrange(len(self.parkingIdList))]


    def get_parking_data(self, parkingId) -> list:
        return self.addedParkingDict[parkingId]


    def get_parking_ids_list(self) -> list:
        return self.parkingIdList


    def getTestEmployeeSupplier(self) -> TestEmployeeSupplier:
        return self.teSupplier


class ParkingDictPOST():
    ''' Создать аналог словаря POST '''


    def create_dict_POST(parkingData: 'list_or_tuple', idEmployee: str='0') -> dict:
        fieldsPOST = parking.ParkingCheckAbstract.PARKING_FIELDS
        dictPOST = {k: '' for k in fieldsPOST}
        for i in range(len(parkingData)):
            dictPOST[fieldsPOST[i]] = parkingData[i]
        if idEmployee != '0':
            dictPOST[fieldsPOST[4]] = idEmployee
        return dictPOST
