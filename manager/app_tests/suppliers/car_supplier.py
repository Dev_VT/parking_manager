from manager.app_tests.suppliers.parking_supplier import TestParkingSupplier
from manager.models import CarBrand, CarModel
from manager.data import car, car_model
from random import randrange
from xml.etree import ElementTree

TEST_BRANDS = ('Tesla', 'Lada')
TEST_MODELS = {
    TEST_BRANDS[0]: ('Model S', 'Model 3', 'Model X'),
    TEST_BRANDS[1]: ('Vesta', 'Granta', 'Kalina', 'Priora'),
}
TEST_CARS = (
    ('ш342ок38', '1', '1', '9', '96000', 'Елизавета', 'Петровская', 'Сергеевна', '77595937809'),
    ('к442пр38', '1', '1', '4', '25000', 'Станислав', 'Васильев', 'Максимович', '+79957437413'),
    ('х111ех138', '1', '1', '2', '30000',  'Вероника', 'Виноградова', 'Ильинична', '87810756910'),
    ('о736са03', '1', '1', '1', '150000', 'Максим', 'Лебедев', 'Владиславович', ''),
    ('в674рп03', '1', '1', '3', '30000',  'Владимир', 'Ларионов', '', '79527511709'),
    ('т000оп138', '1', '1', '6', '150000', 'Анна', 'Кузнецова', 'Дмитриевна', ''),
)


class TestCarModelSupplier():
    def __init__(self):
        self.addedModelDict = dict()
        self.addedBrandDict = dict()
        self.modelIdList = list()
        self.brandIdList = list()


    def add_car_models(self, count: int=-1):
        allModelCount = self._models_count()
        count = allModelCount if count <= -1 or count > allModelCount else count
        brandModelsCounter = 0
        brandCounter = 0
        brand = TEST_BRANDS[brandCounter]
        for i in range(count):
            if i-brandModelsCounter >= len(TEST_MODELS[brand]):
                brandModelsCounter += i
                brandCounter += 1
                brand = TEST_BRANDS[brandCounter]
            modelFields = [TEST_MODELS[brand][i-brandModelsCounter], brand]
            self._set_radio_button(modelFields)
            self._add_model(modelFields)


    def add_car_models_from_file(self, count: int=-1):
        tree = ElementTree.parse('manager/files/car_brands.xml')
        brandTag = tree.getroot()
        addedBrandCount = 0
        for brand in brandTag:
            if addedBrandCount == count:
                break
            brandTitle = brand.attrib['title']
            for model in brand:
                modelFields = [model.text, brandTitle]
                self._set_radio_button(modelFields)
                self._add_model(modelFields)
            addedBrandCount += 1


    def _models_count(self):
        allModelCount = 0
        for brandModels in TEST_MODELS.values():
            allModelCount += len(brandModels)
        return allModelCount


    def _set_radio_button(self, modelFields: list):
        if modelFields[car_model.INDEX_NEW_BRAND] in self.addedBrandDict.values():
            modelFields[car_model.INDEX_NEW_BRAND] = self.brandIdList[-1]
            modelFields.append(car_model.ModelAddNew.NEW_MODEL_RADIO['new_model'])
        else:
            modelFields.append(car_model.ModelAddNew.NEW_MODEL_RADIO['new_brand'])


    def _add_model(self, testModel: list):
        dictPOST = ModelDictPOST.create_dict_POST(testModel)
        m = car_model.ModelAddNew(dictPOST)
        if m.get_status():
            newCarModelModel = m.get_car_model_model()
            self._add_model_to_list(newCarModelModel)


    def _add_model_to_list(self, newCarModelModel):
        newCarModel = [
            newCarModelModel.model,
            newCarModelModel.id_brand.id_brand,
        ]
        self.addedModelDict[newCarModelModel.id_model] = newCarModel
        self.modelIdList.append(newCarModelModel.id_model)
        if not newCarModel[1] in self.brandIdList:
            self._add_brand_to_list(newCarModelModel.id_brand)


    def _add_brand_to_list(self, newCarBrandModel):
        self.addedBrandDict[newCarBrandModel.id_brand] = newCarBrandModel.brand
        self.brandIdList.append(newCarBrandModel.id_brand)


    def get_car_model_id(self, index: int=0) -> int:
        return self.modelIdList[index]


    def get_random_car_model_id(self):
        return self.modelIdList[randrange(len(self.modelIdList))]


    def get_car_brand_id(self, index: int=0) -> int:
        return self.brandIdList[index]


    def get_car_model_data(self, modelId) -> list:
        return self.addedModelDict[modelId]


    def get_car_brand_title(self, brandId) -> str:
        return self.addedBrandDict[brandId]


    def get_car_model_ids_list(self) -> list:
        return self.modelIdList


def add_all_test_cars(idParking: int, idModel: int):
    idParking = str(idParking)
    for addCar in TEST_CARS:
        dictPOST = CarDictPOST.create_dict_POST(addCar, idModel=idModel)
        car.CarAddNew(dictPOST, idParking)


class TestCarSupplier():
    def __init__(self, tpSupplier: TestParkingSupplier=None, tcmSupplier: TestCarModelSupplier=None):
        self.addedCarDict = dict()
        self.carIdList = list()
        self.tpSupplier = tpSupplier
        self.tcmSupplier = tcmSupplier


    def add_cars(self, count: int=-1):
        count = len(TEST_CARS) if count <= -1 or count > len(TEST_CARS) else count
        parkingList = self._add_parking_lots()
        parkingCount = len(parkingList)
        carModelList = self._add_car_models()
        carModelCount = len(carModelList)
        for i in range(count):
            parkingId = parkingList[randrange(parkingCount)]
            carModelId = carModelList[randrange(carModelCount)]
            self._add_car(TEST_CARS[i], parkingId, carModelId)


    def _add_parking_lots(self):
        if self.tpSupplier == None:
            self.tpSupplier = TestParkingSupplier()
            self.tpSupplier.add_parking_lots(2)
        return self.tpSupplier.get_parking_ids_list()


    def _add_car_models(self):
        if self.tcmSupplier == None:
            self.tcmSupplier = TestCarModelSupplier()
            self.tcmSupplier.add_car_models()
        return self.tcmSupplier.get_car_model_ids_list()


    def _add_car(self, testCar: tuple, parkingId: int, carModelId: int):
        testCar = list(testCar)
        testCar[car.INDEX_MODEL] = carModelId
        dictPOST = CarDictPOST.create_dict_POST(testCar)
        c = car.CarAddNew(dictPOST, parkingId)
        if c.get_status():
            newCarModel = c.get_car_model()
            self._add_car_to_list(newCarModel)


    def _add_car_to_list(self, newCarModel):
        newCar = [
            newCarModel.gov_car_number,
            newCarModel.id_number.id_number,
            newCarModel.id_model.id_model,
            newCarModel.color,
            newCarModel.mileage,
            newCarModel.owner_name,
            newCarModel.owner_surname,
            newCarModel.owner_patronymic,
            newCarModel.phone,
        ]
        self.addedCarDict[newCarModel.id_car_number] = newCar
        self.carIdList.append(newCarModel.id_car_number)


    def get_car_id(self, index: int=0):
        return self.carIdList[index]


    def get_car_data(self, carId) -> list:
        return self.addedCarDict[carId]


    def get_test_parking_supplier(self) -> TestParkingSupplier:
        return self.tpSupplier


    def get_test_car_model_supplier(self) -> TestCarModelSupplier:
        return self.tcmSupplier


class ModelDictPOST():
    def create_dict_POST(modelData: 'list_or_tuple') -> dict:
        if len(modelData) == len(car_model.ModelAddNew.MODEL_FIELDS):
            newModelRadioValue = modelData[car_model.INDEX_NEW_MODEL_RADIO]
            if newModelRadioValue == car_model.ModelAddNew.NEW_MODEL_RADIO['new_model']:
                fieldsPOST = car_model.ModelAddNew.MODEL_FIELDS[0]
            elif newModelRadioValue == car_model.ModelAddNew.NEW_MODEL_RADIO['new_brand']:
                fieldsPOST = car_model.ModelAddNew.MODEL_FIELDS[1]
            elif newModelRadioValue == car_model.ModelAddNew.NEW_MODEL_RADIO['old_model']:
                fieldsPOST = car_model.ModelAddNew.MODEL_FIELDS[0]
        else:
            fieldsPOST = car_model.ModelAddNew.MODEL_FIELDS[0]
        fieldsPOST = list(fieldsPOST)
        fieldsPOST.append(car_model.ModelAddNew.MODEL_FIELDS[2])

        dictPOST = {k: '' for k in fieldsPOST}
        for i in range(len(modelData)):
            dictPOST[fieldsPOST[i]] = modelData[i]
        return dictPOST


class CarDictPOST():
    def create_dict_POST(carData: 'list_or_tuple', idModel: int=-1) -> dict:
        fieldsPOST = car.CarCheckAbstract.CAR_FIELDS
        dictPOST = {k: '' for k in fieldsPOST}
        for i in range(len(carData)):
            dictPOST[fieldsPOST[i]] = carData[i]
        if idModel > -1:
            dictPOST[fieldsPOST[car.INDEX_MODEL]] = str(idModel)
        return dictPOST


class SearchDictGET():
    def create_dict_GET(searchData: 'list_or_tuple') -> dict:
        fieldsGET = car.SearchCar.SEARCH_FIELDS
        dictGET = {k: '' for k in fieldsGET}
        for i in range(len(searchData)):
            dictGET[fieldsGET[i]] = searchData[i]
        return dictGET


class TestCarModelCount():
    def get_car_model_count(self):
        addedModels = CarModel.objects.all()
        return addedModels.count()


    def get_car_brand_count(self):
        addedBrands = CarBrand.objects.all()
        return addedBrands.count()
