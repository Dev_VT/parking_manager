from manager.data import employee


TEST_EMPLOYEES = (
    ('Администратор', 'Главный', '', employee.Employee.ADMIN, '74545430966', 'mainadmin'),
    ('Ксения', 'Кузьмина', 'Леонидовна', employee.Employee.SECRETARY, '+70960646811', '555qqq'),
    ('Тимофей', 'Козлов', 'Григорьевич', employee.Employee.ADMIN, '84369929600', 'topANDcool77'),
    ('Михаил', 'Осипов', 'Кириллович', employee.Employee.PARKING_MANAGER, '+76132557828', '111fff'),
    ('Александр', 'Коновалов', 'Андреевич', employee.Employee.PARKING_MANAGER, '70894565957', '888uuui'),
    ('Егор', 'Волков', 'Матвеевич', employee.Employee.PARKING_MANAGER, '81543254046', '888u111'),
)
TEST_PARKING_MANAGERS = TEST_EMPLOYEES[3:6]


class TestEmployeeSupplier():
    def __init__(self):
        self.addedEmployeeDict = dict()
        self.employeeIdList = list()
        self.employeeIdListSort = {
            employee.Employee.ADMIN: list(),
            employee.Employee.SECRETARY: list(),
            employee.Employee.PARKING_MANAGER: list(),
        }


    def add_employees(self, count: int=-1):
        employeeCount = len(TEST_EMPLOYEES)
        count = employeeCount if count <= -1 or count > employeeCount else count
        for testEmployee in TEST_EMPLOYEES[:count]:
            self._add_employee(testEmployee)


    def add_parking_managers(self, count: int=-1):
        parkingManagersCount = len(TEST_PARKING_MANAGERS)
        count = parkingManagersCount if count <= -1 or count > parkingManagersCount else count
        for testEmployee in TEST_PARKING_MANAGERS[:count]:
            self._add_employee(testEmployee)


    def _add_employee(self, testEmployee):
        dictPOST = EmployeeDictPOST.create_dict_POST(testEmployee, addPasswords=True)
        e = employee.EmployeeAddNew(dictPOST)
        if e.get_status():
            newEmployeeModel = e.get_employee_model()
            self._add_employee_to_list(newEmployeeModel)


    def _add_employee_to_list(self, newEmployeeModel):
        newEmployee = [
            newEmployeeModel.name,
            newEmployeeModel.surname,
            newEmployeeModel.patronymic,
            newEmployeeModel.position,
            newEmployeeModel.phone,
            newEmployeeModel.password,
        ]
        self.addedEmployeeDict[newEmployeeModel.id_employee] = newEmployee
        self.employeeIdList.append(newEmployeeModel.id_employee)
        self.employeeIdListSort[newEmployeeModel.position].append(newEmployeeModel.id_employee)


    def get_employee_data(self, employeeId) -> list:
        return self.addedEmployeeDict[employeeId]


    def get_employee_id(self, index: int=0):
        return self.employeeIdList[index]


    def get_admin_id(self, index: int=0):
        return self.employeeIdListSort[employee.Employee.ADMIN][index]


    def get_secretary_id(self, index: int=0):
        return self.employeeIdListSort[employee.Employee.SECRETARY][index]


    def get_parking_manager_id(self, index: int=0):
        return self.employeeIdListSort[employee.Employee.PARKING_MANAGER][index]


    def get_parking_manager_ids_list(self):
        return self.employeeIdListSort[employee.Employee.PARKING_MANAGER]


class EmployeeDictPOST():
    ''' Создать аналог словаря POST '''


    def create_dict_POST(employeeData: 'list_or_tuple', addPasswords: bool=False, password: str=None) -> dict:
        fieldsPOST = employee.EmployeeCheckAbstract.EMPLOYEE_FIELDS
        dictPOST = {k: '' for k in fieldsPOST}
        for i in range(len(employeeData)):
            dictPOST[fieldsPOST[i]] = employeeData[i]
        if addPasswords:
            EmployeeDictPOST._add_two_passwords(dictPOST, password)
        return dictPOST


    def _add_two_passwords(dictPOST: dict, password: str=None) -> dict:
        passwordField = employee.EmployeeCheckAbstract.EMPLOYEE_FIELDS[employee.INDEX_PASSWORD]
        passwordRepeatField = employee.EmployeeCheckAbstract.EMPLOYEE_FIELDS[employee.INDEX_REPEAT_PASSWORD]
        if password != None:
            dictPOST[passwordField] = password # поле ввода пароля
            dictPOST[passwordRepeatField] = password # поле ввода повторения пароля
        else:
            dictPOST[passwordRepeatField] = dictPOST[passwordField] # продублировать уже присутствующий пароль
        return dictPOST
