from django.test import TestCase
from manager.app_tests.suppliers.employee_supplier import TestEmployeeSupplier, TEST_EMPLOYEES
from manager.app_tests.suppliers.parking_supplier import TestParkingSupplier
from manager.app_tests.suppliers.car_supplier import TestCarSupplier
from django.urls import reverse
from manager.data import employee, parking


class FirstLoginTest(TestCase):
    def test_first_login_check(self):
        ''' Проверка создания первоначальных данных '''
        testUrl = 'login_check'
        adminData = TEST_EMPLOYEES[0]
        adminPassword = adminData[employee.INDEX_PASSWORD]
        dictPOST = {'password_field': adminPassword}
        # вход с паролем по умолчанию
        response = self.client.post(reverse(testUrl), dictPOST, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.redirect_chain[0], (reverse('main_page'), 302))
        # уже вошедши ещё раз посещаем страницу проверки пароля, которая должна перенести на главную
        response = self.client.get(reverse(testUrl), follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.redirect_chain[0], (reverse('main_page'), 302))


class PageRestrictionsTest(TestCase):
    ''' Тестирование доступа для сотрудников на страницах '''
    CREATE_GET_DICT_FROM = 'f'
    CREATE_GET_DICT_TO = 't'


    def setUp(self):
        self.getFromDict = None
        self.getToDict = None
        self.teSupplier = TestEmployeeSupplier()
        self.teSupplier.add_employees()
        self.tpSupplier = TestParkingSupplier(self.teSupplier)
        self.tpSupplier.add_parking_lots(1)
        self.tcSupplier = TestCarSupplier(self.tpSupplier)
        self.tcSupplier.add_cars(3)


    def _add_second_parking(self):
        self.tpSupplier.add_next_parking()


    def test_login_page_without_login(self):
        testUrl = 'login_page'
        self._then_not_redirect(testUrl)
        testUrl = 'index'
        self._then_redirect(testUrl, 'login_page')


    def test_login_check(self):
        testUrl = 'login_check'
        wrongPassword = 'wr0ngPa55'
        dictPOST = {'password_field': wrongPassword}
        response = self.client.post(reverse(testUrl), dictPOST, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.redirect_chain[0], (reverse('login_page'), 302))

        adminData = TEST_EMPLOYEES[0]
        adminPassword = adminData[employee.INDEX_PASSWORD]
        dictPOST = {'password_field': adminPassword}
        response = self.client.post(reverse(testUrl), dictPOST, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.redirect_chain[0], (reverse('main_page'), 302))
        # уже вошедши ещё раз посещаем страницу проверки пароля, которая должна перенести на главную
        self._then_redirect(testUrl, 'main_page')


    def test_login_page_with_login(self):
        testUrl = 'login_check'
        self._login_admin()
        self._then_redirect(testUrl, 'main_page')


    def test_main_page(self):
        testUrl = 'main_page'
        self._access_close_for_parking_manager(testUrl)
        self._access_open_for_administrator_and_secretary(testUrl)


    def test_employees_list_page(self):
        testUrl = 'employees_list_page'
        self._access_open_only_for_administrator(testUrl)


    def test_employee_page(self):
        testUrl = 'employee_page'
        # страница добавления сотрудника
        self._access_open_only_for_administrator(testUrl)
        # страница изменения сотрудника
        secretaryId = self.teSupplier.get_secretary_id()
        self._create_GET_dict(self.CREATE_GET_DICT_FROM, id_employee = secretaryId)
        self._access_open_only_for_administrator(testUrl)


    def test_employee_create(self):
        testUrl = 'employee_create'
        self._access_close_for_secretary_and_parking_manager(testUrl)
        # попытка добавления сотрудника без передачи данных, вернет на страницу добавления
        self._login_admin()
        self._then_redirect(testUrl, 'employee_page')


    def test_employee_update(self):
        testUrl = 'employee_update'
        secretaryId = self.teSupplier.get_secretary_id()
        self._create_GET_dict(self.CREATE_GET_DICT_FROM, id_employee = secretaryId)
        self._access_close_for_secretary_and_parking_manager(testUrl)
        # при отсутствии обновляемых данных, переход обратно на страницу обновления
        self._create_GET_dict(self.CREATE_GET_DICT_TO, id_employee = secretaryId)
        self._login_admin()
        self._then_redirect(testUrl, 'employee_page')


    def test_employee_delete(self):
        testUrl = 'employee_delete'
        secretaryId = self.teSupplier.get_secretary_id()
        self._create_GET_dict(self.CREATE_GET_DICT_FROM, id_employee = secretaryId)
        self._access_close_for_secretary_and_parking_manager(testUrl)
        # при удалении возврат на страницу сотрудников
        self._create_GET_dict(self.CREATE_GET_DICT_FROM, id_employee = secretaryId)
        self._login_admin()
        self._then_redirect(testUrl, 'employees_list_page')


    def test_parking_list_page(self):
        testUrl = 'parking_list_page'
        self._access_close_for_parking_manager(testUrl)
        self._access_open_for_administrator_and_secretary(testUrl)


    def test_parking_page(self):
        testUrl = 'parking_page'
        # страница добавления стоянки
        self._access_close_for_parking_manager(testUrl)
        self._access_open_for_administrator_and_secretary(testUrl)
        # страница изменения стоянки
        parkingId = self.tpSupplier.get_parking_id()
        self._create_GET_dict(self.CREATE_GET_DICT_FROM, id_parking = parkingId)
        self._access_close_for_parking_manager(testUrl)
        self._access_open_for_administrator_and_secretary(testUrl)


    def test_parking_create(self):
        testUrl = 'parking_create'
        self._access_close_for_parking_manager(testUrl)
        # попытка добавления стоянки без передачи данных
        self._login_admin()
        self._then_redirect(testUrl, 'parking_page')
        self._login_secretary()
        self._then_redirect(testUrl, 'parking_page')


    def test_parking_update(self):
        testUrl = 'parking_update'
        parkingId = self.tpSupplier.get_parking_id()
        self._create_GET_dict(self.CREATE_GET_DICT_FROM, id_parking = parkingId)
        self._access_close_for_parking_manager(testUrl)
        # при отсутствии обновляемых данных, переход обратно на страницу обновления
        self._create_GET_dict(self.CREATE_GET_DICT_TO, id_parking = parkingId)
        self._login_admin()
        self._then_redirect(testUrl, 'parking_page')
        self._login_secretary()
        self._then_redirect(testUrl, 'parking_page')


    def test_parking_delete(self):
        self._add_second_parking()
        testUrl = 'parking_delete'
        parking1Id = self.tpSupplier.get_parking_id(0)
        parking2Id = self.tpSupplier.get_parking_id(1)
        self._create_GET_dict(self.CREATE_GET_DICT_FROM, id_parking = parking1Id)
        self._access_close_for_parking_manager(testUrl)
        # при удалении возврат на страницу стоянок
        self._create_GET_dict(self.CREATE_GET_DICT_FROM, id_parking = parking1Id)
        self._login_admin()
        self._then_redirect(testUrl, 'parking_list_page')
        self._create_GET_dict(self.CREATE_GET_DICT_FROM, id_parking = parking2Id)
        self._login_secretary()
        self._then_redirect(testUrl, 'parking_list_page')


    def test_car_list_page(self):
        testUrl = 'car_list_page'
        parkingId = self.tpSupplier.get_parking_id()
        self._create_GET_dict(self.CREATE_GET_DICT_FROM, id_parking = parkingId)
        self._access_close_for_quest(testUrl)
        self._access_open_for_all_employees(testUrl)


    def test_define_parking_manager_page(self):
        testUrl = 'define_parking_manager_page'
        self._access_close_for_quest(testUrl)
        self._access_close_for_administrator_and_secretary(testUrl)
        # управляющий без стоянки отправляется на пустую страницу
        employeeId = self.teSupplier.get_parking_manager_id(1)
        self._login_id(employeeId)
        self._then_redirect(testUrl, 'have_not_parking_page')
        # управляющий со стоянкой отправляется к себе на страницу
        parkingId = self.tpSupplier.get_parking_id()
        parkingData = self.tpSupplier.get_parking_data(parkingId)
        self._login_id(parkingData[parking.INDEX_MANAGER])
        self._create_GET_dict(self.CREATE_GET_DICT_TO, id_parking = parkingId)
        self._then_redirect(testUrl, 'car_list_page')


    def test_car_page(self):
        testUrl = 'car_page'
        parkingId = self.tpSupplier.get_parking_id()
        # страница добавления авто
        self._create_GET_dict(self.CREATE_GET_DICT_FROM, id_parking = parkingId)
        self._access_close_for_quest(testUrl)
        self._access_open_for_all_employees(testUrl)
        # страница изменения авто
        carId = self.tcSupplier.get_car_id()
        self._create_GET_dict(self.CREATE_GET_DICT_FROM, id_car = carId)
        self._access_close_for_quest(testUrl)
        self._access_open_for_all_employees(testUrl)


    def test_car_create(self):
        testUrl = 'car_create'
        parkingId = self.tpSupplier.get_parking_id()
        self._create_GET_dict(self.CREATE_GET_DICT_FROM, id_parking = parkingId)
        self._access_close_for_quest(testUrl)
        # попытка добавления авто без передачи данных
        self._create_GET_dict(self.CREATE_GET_DICT_TO, id_parking = parkingId)
        self._login_admin()
        self._then_redirect(testUrl, 'car_page')
        self._login_secretary()
        self._then_redirect(testUrl, 'car_page')
        self._login_parking_manager()
        self._then_redirect(testUrl, 'car_page')


    def test_car_update(self):
        testUrl = 'car_update'
        carId = self.tcSupplier.get_car_id()
        self._create_GET_dict(self.CREATE_GET_DICT_FROM, id_car = carId)
        self._access_close_for_quest(testUrl)
        # при отсутствии обновляемых данных, переход обратно на страницу обновления
        self._create_GET_dict(self.CREATE_GET_DICT_TO, id_car = carId)
        self._login_admin()
        self._then_redirect(testUrl, 'car_page')
        self._login_secretary()
        self._then_redirect(testUrl, 'car_page')
        self._login_parking_manager()
        self._then_redirect(testUrl, 'car_page')


    def test_car_delete(self):
        testUrl = 'car_delete'
        parkingId = self.tpSupplier.get_parking_id()
        car1Id = self.tcSupplier.get_car_id(0)
        car2Id = self.tcSupplier.get_car_id(1)
        car3Id = self.tcSupplier.get_car_id(2)
        self._create_GET_dict(self.CREATE_GET_DICT_FROM, id_car = car1Id)
        self._access_close_for_quest(testUrl)
        # при удалении возврат на страницу машин на стоянке
        self._create_GET_dict(self.CREATE_GET_DICT_TO, id_parking = parkingId)
        self._create_GET_dict(self.CREATE_GET_DICT_FROM, id_car = car1Id)
        self._login_admin()
        self._then_redirect(testUrl, 'car_list_page')
        self._create_GET_dict(self.CREATE_GET_DICT_FROM, id_car = car2Id)
        self._login_secretary()
        self._then_redirect(testUrl, 'car_list_page')
        self._create_GET_dict(self.CREATE_GET_DICT_FROM, id_car = car3Id)
        self._login_parking_manager()
        self._then_redirect(testUrl, 'car_list_page')


    def test_car_search_page(self):
        testUrl = 'car_search_page'
        self._access_close_for_parking_manager(testUrl)
        self._access_open_for_administrator_and_secretary(testUrl)


    def test_document_parking_list(self):
        testUrl = 'manager:document_parking_list'
        self._access_close_for_parking_manager(testUrl)
        self._access_open_for_administrator_and_secretary(testUrl)


    def test_document_parking_list(self):
        testUrl = 'manager:document_car_list'
        self._access_close_for_quest(testUrl)
        self._access_open_for_all_employees(testUrl)


    def test_document_parking_list(self):
        testUrl = 'manager:document_all_car_list'
        self._access_close_for_parking_manager(testUrl)
        self._access_open_for_administrator_and_secretary(testUrl)


    ''' ------ Вспомогательные функции ------ '''
    def _login_admin(self):
        adminId = self.teSupplier.get_admin_id()
        self._login_id(adminId)


    def _login_secretary(self):
        secretaryId = self.teSupplier.get_secretary_id()
        self._login_id(secretaryId)


    def _login_parking_manager(self):
        parkingManagerId = self.teSupplier.get_parking_manager_id()
        self._login_id(parkingManagerId)


    def _login_id(self, employeeId: int):
        employeeData = self.teSupplier.get_employee_data(employeeId)
        employeePassword = employeeData[employee.INDEX_PASSWORD]
        self._login_password(employeePassword)


    def _login_password(self, password: str):
        dictPOST = {'password_field': password}
        response = self.client.post(reverse('login_check'), dictPOST)


    def _logout(self):
        self.client.get(reverse('logout'))


    def _create_GET_dict(self, isFromOrTo: str, **getDict: dict):
        ''' Словарь с данными для ссылки типа GET '''
        if isFromOrTo == self.CREATE_GET_DICT_FROM:
            self.getFromDict = getDict
        elif isFromOrTo == self.CREATE_GET_DICT_TO:
            self.getToDict = getDict


    def _then_redirect(self, fromUrl: str, toUrl: str):
        fromUrl = reverse(fromUrl, kwargs=self.getFromDict)
        toUrl = reverse(toUrl, kwargs=self.getToDict)
        response = self.client.get(fromUrl, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.redirect_chain[0], (toUrl, 302))
        self._logout()


    def _then_not_redirect(self, fromUrl: str):
        fromUrl = reverse(fromUrl, kwargs=self.getFromDict)
        response = self.client.get(fromUrl, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(len(response.redirect_chain) == 0)
        self._logout()


    def _access_open_only_for_administrator(self, url):
        self._access_close_for_secretary_and_parking_manager(url)
        self._login_admin()
        self._then_not_redirect(url)


    def _access_open_for_administrator_and_secretary(self, url):
        self._login_admin()
        self._then_not_redirect(url)
        self._login_secretary()
        self._then_not_redirect(url)


    def _access_open_for_all_employees(self, url):
        self._access_open_for_administrator_and_secretary(url)
        self._login_parking_manager()
        self._then_not_redirect(url)


    def _access_close_for_administrator_and_secretary(self, url):
        self._login_admin()
        self._then_redirect(url, 'main_page')
        self._login_secretary()
        self._then_redirect(url, 'main_page')


    def _access_close_for_secretary_and_parking_manager(self, url):
        self._access_close_for_parking_manager(url)
        self._login_secretary()
        self._then_redirect(url, 'main_page')


    def _access_close_for_parking_manager(self, url):
        self._access_close_for_quest(url)
        self._login_parking_manager()
        self._then_redirect(url, 'define_parking_manager_page')


    def _access_close_for_quest(self, url):
        # доступ не вошедшему пользователю запрещен
        self._then_redirect(url, 'login_page')
