from manager.app_tests.error_checking import ErrorCheckingTestCase
from django.test import TestCase
from manager.app_tests.suppliers.employee_supplier import *
from manager.app_tests.suppliers.parking_supplier import TestParkingSupplier
from manager.data import employee, login


class CreateEmployeeTest(ErrorCheckingTestCase):
    # override
    def create_testing_object(self, dictPOST: dict):
        return employee.EmployeeAddNew(dictPOST)


    def test_empty_fields(self):
        givenDictPOSTs = ErrorCheckingTestCase.given_dict_POST_from_empty_fields_check(
            EmployeeDictPOST.create_dict_POST, TEST_EMPLOYEES[1], employee.INDEX_REPEAT_PASSWORD)
        self.then_many_error_status(givenDictPOSTs, 'error_field')


    def test_not_right_position(self):
        fields = list(TEST_EMPLOYEES[1])
        fields[employee.INDEX_POSITION] = 'j' # некорректная позиция
        dictPOST = EmployeeDictPOST.create_dict_POST(fields, addPasswords=True)
        self.then_error_status(dictPOST, 'error_position')


    def test_not_right_phone(self):
        rightFields = list(TEST_EMPLOYEES[1])
        givenDictPOSTs = CreateEmployeeTest.given_wrong_phone(rightFields)
        self.then_many_error_status(givenDictPOSTs, 'error_phone')


    def given_wrong_phone(rightFields: list) -> list:
        givenDictPOSTs = list()
        wrongPhones = ('123', 'Не_числоооо', '8900123123+')
        for phone in wrongPhones:
            wrongPhoneFields = rightFields.copy()
            wrongPhoneFields[employee.INDEX_PHONE] = phone
            dictPOST = EmployeeDictPOST.create_dict_POST(wrongPhoneFields, addPasswords=True)
            givenDictPOSTs.append(dictPOST)
        return givenDictPOSTs


    def test_not_equal_passwords(self):
        fields = list(TEST_EMPLOYEES[1])
        fields.append('Добавляем несовпадающий пароль')
        dictPOST = EmployeeDictPOST.create_dict_POST(fields)
        self.then_error_status(dictPOST, 'error_not_equal_passwords')


    def test_length_passwords(self):
        withoutPasswordFields = list(TEST_EMPLOYEES[1][:employee.INDEX_PHONE+1])
        givenDictPOSTs = (
            EmployeeDictPOST.create_dict_POST(withoutPasswordFields, addPasswords=True, password='short'),
            EmployeeDictPOST.create_dict_POST(withoutPasswordFields, addPasswords=True, password='very_very_longgg_password'),
        )
        self.then_many_error_status(givenDictPOSTs, 'error_length_password')


    def test_add_employee(self):
        dictPOST = EmployeeDictPOST.create_dict_POST(TEST_EMPLOYEES[1], addPasswords=True)
        self.then_error_status(dictPOST, 'not_errors')


    def test_not_unique_phone(self):
        teManager = TestEmployeeSupplier()
        teManager.add_employees(1)
        idEmployee = teManager.get_employee_id()
        testEmployee = teManager.get_employee_data(idEmployee)
        repeatDictPOST = EmployeeDictPOST.create_dict_POST(testEmployee, addPasswords=True)
        self.then_error_status(repeatDictPOST, 'error_phone_used')


    def test_not_unique_password(self):
        teManager = TestEmployeeSupplier()
        teManager.add_employees(1)
        idEmployee = teManager.get_employee_id()
        testEmployee = teManager.get_employee_data(idEmployee)
        testEmployee[employee.INDEX_PHONE] = UpdateEmployeeTest.TEST_UPDATE_EMPLOYEE[employee.INDEX_PHONE]
        repeatDictPOST = EmployeeDictPOST.create_dict_POST(testEmployee, addPasswords=True)
        self.then_error_status(repeatDictPOST, 'error_password_used')


    def test_auto_create_main_administrator(self):
        adminPassword = TEST_EMPLOYEES[0][employee.INDEX_PASSWORD]
        l = login.Login(adminPassword)
        self.assertTrue(l.is_login_correct())


class UpdateEmployeeTest(ErrorCheckingTestCase):
    TEST_UPDATE_EMPLOYEE = (
        'Измененное имя', 'Измененная фамилия', 'Измененное отчество',
        employee.Employee.PARKING_MANAGER, '79001231231', 'qqq6667u7')


    def setUp(self):
        self.teManager = TestEmployeeSupplier()
        self.teManager.add_employees(4)
        self.idUpdatingEmployee = self.teManager.get_secretary_id()
        self.idAdmin = self.teManager.get_admin_id()
        self.idParkingManager = self.teManager.get_parking_manager_id()


    # override
    def create_testing_object(self, dictPOST: dict):
        return employee.EmployeeUpdate(dictPOST, self.idUpdatingEmployee, self.idAdmin)


    def test_get_updating_employee_values_from_db(self):
        values = employee.EmployeeCheckAbstract.EMPLOYEE_VALUES
        employeeValues = self.teManager.get_employee_data(self.idUpdatingEmployee)
        employeeValues = employeeValues[:employee.INDEX_PHONE+1]
        rightDict = {values[i]: employeeValues[i] for i in range(len(employeeValues))}
        testDict = employee.Employee.get_employee_values(self.idUpdatingEmployee)
        self.assertTrue(testDict == rightDict)


    def test_empty_fields(self):
        givenDictPOSTs = ErrorCheckingTestCase.given_dict_POST_from_empty_fields_check(
            EmployeeDictPOST.create_dict_POST, TEST_EMPLOYEES[1], employee.INDEX_PHONE)
        self.then_many_error_status(givenDictPOSTs, 'error_field')


    def test_not_unique_phone(self):
        wrongPhoneFields = list(self.TEST_UPDATE_EMPLOYEE[:employee.INDEX_PHONE+1])
        testEmployee = self.teManager.get_employee_data(self.idAdmin)
        wrongPhoneFields[employee.INDEX_PHONE] = testEmployee[employee.INDEX_PHONE]
        dictPOST = EmployeeDictPOST.create_dict_POST(wrongPhoneFields, addPasswords=True)
        self.then_error_status(dictPOST, 'error_phone_used')


    def test_not_equal_passwords(self):
        withoutRepeatPasswordFields = self.TEST_UPDATE_EMPLOYEE # поле 'Повторите пароль' пустое
        notEqualPasswordFields = list(self.TEST_UPDATE_EMPLOYEE)
        notEqualPasswordFields.append('Добавляем несовпадающий пароль')
        givenDictPOSTs = (
            EmployeeDictPOST.create_dict_POST(withoutRepeatPasswordFields),
            EmployeeDictPOST.create_dict_POST(notEqualPasswordFields),
        )
        self.then_many_error_status(givenDictPOSTs, 'error_not_equal_passwords')


    def test_not_unique_password(self):
        notUniqueFields = list(self.TEST_UPDATE_EMPLOYEE)
        employeeData = self.teManager.get_employee_data(self.idAdmin)
        notUniqueFields[employee.INDEX_PASSWORD] = employeeData[employee.INDEX_PASSWORD]
        dictPOST = EmployeeDictPOST.create_dict_POST(notUniqueFields, addPasswords=True)
        self.then_error_status(dictPOST, 'error_password_used')


    def test_update_employee_without_password(self):
        rightFields = self.TEST_UPDATE_EMPLOYEE[:employee.INDEX_PHONE+1]
        dictPOST = EmployeeDictPOST.create_dict_POST(rightFields)
        self.then_error_status(dictPOST, 'not_errors')


    def test_update_employee_with_password(self):
        rightFields = self.TEST_UPDATE_EMPLOYEE
        dictPOST = EmployeeDictPOST.create_dict_POST(rightFields, addPasswords=True)
        self.then_error_status(dictPOST, 'not_errors')


    def test_update_admin_position(self):
        self.idUpdatingEmployee = self.idAdmin
        wrongPositionFields = self.teManager.get_employee_data(self.idUpdatingEmployee)
        wrongPositionFields = wrongPositionFields[:employee.INDEX_PHONE+1]
        wrongPositionFields[employee.INDEX_POSITION] = employee.Employee.SECRETARY
        dictPOST = EmployeeDictPOST.create_dict_POST(wrongPositionFields)
        self.then_error_status(dictPOST, 'error_admin_position')


    def test_update_parking_manager_position_with_parking(self):
        tpSupplier = TestParkingSupplier(self.teManager)
        tpSupplier.add_parking_lots(1)
        self.idUpdatingEmployee = self.idParkingManager
        wrongPositionFields = self.teManager.get_employee_data(self.idUpdatingEmployee)
        wrongPositionFields = wrongPositionFields[:employee.INDEX_PHONE+1]
        wrongPositionFields[employee.INDEX_POSITION] = employee.Employee.SECRETARY
        dictPOST = EmployeeDictPOST.create_dict_POST(wrongPositionFields)
        self.then_error_status(dictPOST, 'error_manager_position')


class EmployeeListTest(TestCase):
    def test_empty_list(self):
        eList = employee.EmployeesList()
        self.assertEqual(0, eList.get_size())


    def test_employee_list(self):
        teManager = TestEmployeeSupplier()
        teManager.add_employees()
        eList = employee.EmployeesList()
        self.assertEqual(len(TEST_EMPLOYEES), eList.get_size())


    def test_list_without_passwords(self):
        ''' Список не должен содержать пароли сотрудников '''
        teManager = TestEmployeeSupplier()
        teManager.add_employees()
        values = employee.EmployeeCheckAbstract.EMPLOYEE_VALUES
        countFieldsWithoutPassword = len(values) - 1
        eList = employee.EmployeesList()
        eList = eList.get_list()
        for i in range(len(TEST_EMPLOYEES)):
            self.assertEqual(countFieldsWithoutPassword, len(eList[i]))


    def test_set_positions(self):
        teManager = TestEmployeeSupplier()
        teManager.add_employees()
        positions = employee.Employee.get_position_choices()
        strPositions = [t[1] for t in positions]
        eList = employee.EmployeesList()
        eList = eList.get_list()
        for i in range(len(strPositions)):
            employeePosition = eList[i][employee.INDEX_POSITION + 1]
            self.assertTrue(employeePosition in strPositions)


class DeleteEmployeesTest(ErrorCheckingTestCase):
    def setUp(self):
        self.teManager = TestEmployeeSupplier()
        self.teManager.add_employees()
        self.idAdmin = self.teManager.get_admin_id()
        self.idDeleteEmployee = None


    def create_testing_object(self, dictPOST: dict):
        return employee.EmployeeDelete(self.idDeleteEmployee, self.idAdmin)


    def test_delete_myself(self):
        self.idDeleteEmployee = self.idAdmin
        self.then_error_status(None, 'error_self')


    def test_delete_secretary(self):
        self.idDeleteEmployee = self.teManager.get_secretary_id()
        self.then_error_status(None, 'not_errors')


    def test_delete_parking_manager_without_parking(self):
        self.idDeleteEmployee = self.teManager.get_parking_manager_id()
        self.then_error_status(None, 'not_errors')


    def test_delete_parking_manager_with_parking(self):
        tpSupplier = TestParkingSupplier(self.teManager)
        tpSupplier.add_parking_lots(1)
        self.idDeleteEmployee = self.teManager.get_parking_manager_id()
        self.then_error_status(None, 'error_parking')


    def test_delete_second_admin(self):
        self.idDeleteEmployee = self.teManager.get_admin_id(1)
        self.then_error_status(None, 'not_errors')


class ManagersListTest(TestCase):
    def test_empty_list(self):
        mList = employee.ManagersList()
        self._then_empty_list(mList)


    def test_empty_list_without_managers(self):
        teManager = TestEmployeeSupplier()
        teManager.add_employees(2)
        self.test_empty_list()


    def test_list_with_manager(self):
        teManager = TestEmployeeSupplier()
        teManager.add_parking_managers(1)
        mList = employee.ManagersList()
        self._then_list_1_item(mList)


    def test_list_with_manager_2(self):
        teManager = TestEmployeeSupplier()
        teManager.add_parking_managers(1)
        tpSupplier = TestParkingSupplier(teManager)
        tpSupplier.add_parking_lots(1)
        mList = employee.ManagersList()
        self._then_list_1_item(mList)


    def test_list_with_two_managers(self):
        teManager = TestEmployeeSupplier()
        teManager.add_parking_managers(2)
        tpSupplier = TestParkingSupplier(teManager)
        tpSupplier.add_parking_lots(1)
        mList = employee.ManagersList(freeManagers=True)
        self._then_list_1_item(mList)


    def test_list_not_free_managers(self):
        teManager = TestEmployeeSupplier()
        teManager.add_parking_managers(1)
        tpSupplier = TestParkingSupplier(teManager)
        tpSupplier.add_parking_lots(1)
        mList = employee.ManagersList(freeManagers=True)
        self._then_empty_list(mList)


    def _then_empty_list(self, mList: employee.ManagersList):
        self.assertEqual(0, mList.get_size())
        self._then_error_status(mList, 'error_manager_list')


    def _then_list_1_item(self, mList: employee.ManagersList):
        self.assertEqual(1, mList.get_size())
        self._then_error_status(mList, 'not_errors')


    def _then_error_status(self, mList: employee.ManagersList, errStatus: str):
        self.assertEqual(mList.get_status(), _what_status_should_be(errStatus))
        self.assertEqual(mList.err_status, mList.ERR_STATUS[errStatus])


def _what_status_should_be(errStatus: str) -> bool:
    return True if errStatus == 'not_errors' else False
