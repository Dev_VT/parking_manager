from manager.app_tests.error_checking import ErrorCheckingTestCase
from django.test import TestCase
from manager.app_tests.suppliers.car_supplier import *
from manager.app_tests.suppliers.parking_supplier import TestParkingSupplier
from manager.data import car, car_model, first_login


TEST_VALUES = (
    (TEST_MODELS[TEST_BRANDS[0]][0], TEST_BRANDS[0], car_model.ModelAddNew.NEW_MODEL_RADIO['new_brand']), # добавляется модель и марка
    (TEST_MODELS[TEST_BRANDS[0]][1], '1', car_model.ModelAddNew.NEW_MODEL_RADIO['new_model']), # добавляется модель к имеющейся марке
    ('1', '', car_model.ModelAddNew.NEW_MODEL_RADIO['old_model']), # модель выбирается из имеющихся
)


class CreateModelTest(ErrorCheckingTestCase):
    def setUp(self):
        self.carModelCount = TestCarModelCount()


    # override
    def create_testing_object(self, dictPOST: dict):
        return car_model.ModelAddNew(dictPOST)


    def test_need_add_new_model(self):
        yesFields1 = ModelDictPOST.create_dict_POST(TEST_VALUES[0])
        yesFields2 = ModelDictPOST.create_dict_POST(TEST_VALUES[1])
        notFields = ModelDictPOST.create_dict_POST(TEST_VALUES[2])
        self.assertTrue(car_model.ModelAddNew.need_add_new_model(yesFields1))
        self.assertTrue(car_model.ModelAddNew.need_add_new_model(yesFields2))
        self.assertFalse(car_model.ModelAddNew.need_add_new_model(notFields))


    def test_empty_fields(self):
        givenDictPOSTs = ErrorCheckingTestCase.given_dict_POST_from_empty_fields_check(
            ModelDictPOST.create_dict_POST, TEST_VALUES[0], car_model.INDEX_NEW_BRAND)
        self.then_many_error_status(givenDictPOSTs, 'error_field')


    def test_int_fields(self):
        wrongIntFields = {
            car_model.INDEX_NEW_BRAND: 'Марка',
        }
        givenDictPOSTs = ErrorCheckingTestCase.given_dict_POST_from_int_fields_check(
            ModelDictPOST.create_dict_POST, TEST_VALUES[1], wrongIntFields)
        self.then_many_error_status(givenDictPOSTs, 'error_not_int')


    def test_add_new_model_and_brand(self):
        newBrandFields = TEST_VALUES[0]
        dictPOST = ModelDictPOST.create_dict_POST(newBrandFields)
        self.then_error_status(dictPOST, 'not_errors')
        self.assertEqual(1, self.carModelCount.get_car_brand_count())
        self.assertEqual(1, self.carModelCount.get_car_model_count())


    def test_add_only_new_model(self):
        brandId = self._given_one_car_brand()
        newModelFields = list(TEST_VALUES[1])
        newModelFields[car_model.INDEX_NEW_BRAND] = brandId
        dictPOST = ModelDictPOST.create_dict_POST(newModelFields)
        self.then_error_status(dictPOST, 'not_errors')
        self.assertEqual(1, self.carModelCount.get_car_brand_count())
        self.assertEqual(2, self.carModelCount.get_car_model_count())


    def test_add_new_model_with_brand_repeat(self):
        self._given_one_car_brand()
        brandRepeatFields = list(TEST_VALUES[0])
        brandRepeatFields[car_model.INDEX_NEW_MODEL] = TEST_VALUES[1][0]
        dictPOST = ModelDictPOST.create_dict_POST(brandRepeatFields)
        self.then_error_status(dictPOST, 'not_errors')
        self.assertEqual(1, self.carModelCount.get_car_brand_count())
        self.assertEqual(2, self.carModelCount.get_car_model_count())


    def test_wrong_brand(self):
        brandId = self._given_one_car_brand()
        wrongBrandFields = list(TEST_VALUES[1])
        wrongBrandFields[car_model.INDEX_NEW_BRAND] = str(brandId + 30)
        dictPOST = ModelDictPOST.create_dict_POST(wrongBrandFields)
        self.then_error_status(dictPOST, 'error_brand_not_found')


    def test_model_repeat(self):
        modelData = self._given_one_car_model()
        modelRepeatFields = list(TEST_VALUES[1])
        modelRepeatFields[car_model.INDEX_NEW_MODEL] = modelData[car_model.INDEX_NEW_MODEL]
        modelRepeatFields[car_model.INDEX_NEW_BRAND] = str(modelData[car_model.INDEX_NEW_BRAND])
        dictPOST = ModelDictPOST.create_dict_POST(modelRepeatFields)
        self.then_error_status(dictPOST, 'error_model_already_exists')
        self.assertEqual(1, self.carModelCount.get_car_brand_count())
        self.assertEqual(1, self.carModelCount.get_car_model_count())


    def test_model_repeat_with_brand_repeat(self):
        tcmSupplier = TestCarModelSupplier()
        tcmSupplier.add_car_models(1)
        dublicateFields = TEST_VALUES[0]
        dictPOST = ModelDictPOST.create_dict_POST(dublicateFields)
        self.then_error_status(dictPOST, 'error_model_already_exists')
        self.assertEqual(1, self.carModelCount.get_car_brand_count())
        self.assertEqual(1, self.carModelCount.get_car_model_count())


    def _given_one_car_brand(self):
        tcmSupplier = TestCarModelSupplier()
        tcmSupplier.add_car_models(1)
        return tcmSupplier.get_car_brand_id()


    def _given_one_car_model(self):
        tcmSupplier = TestCarModelSupplier()
        tcmSupplier.add_car_models(1)
        modelId = tcmSupplier.get_car_model_id()
        return tcmSupplier.get_car_model_data(modelId)


class CreateCarTest(ErrorCheckingTestCase):
    def setUp(self):
        self.tpSupplier = TestParkingSupplier()
        self.tpSupplier.add_parking_lots(1)
        self.parkingId = self.tpSupplier.get_parking_id()


    # override
    def create_testing_object(self, dictPOST: dict):
        return car.CarAddNew(dictPOST, self.parkingId)


    def test_empty_fields(self):
        givenDictPOSTs = ErrorCheckingTestCase.given_dict_POST_from_empty_fields_check(
            CarDictPOST.create_dict_POST, TEST_CARS[0], car.INDEX_OWNER_SURNAME)
        self.then_many_error_status(givenDictPOSTs, 'error_field')


    def test_int_fields(self):
        givenValues = CreateCarTest.given_wrong_int_fields()
        givenDictPOSTs = ErrorCheckingTestCase.given_dict_POST_from_int_fields_check(
            CarDictPOST.create_dict_POST, TEST_CARS[0], givenValues)
        self.then_many_error_status(givenDictPOSTs, 'error_not_int')


    def given_wrong_int_fields() -> dict:
        return {
            car.INDEX_MODEL: 'Модель',
            car.INDEX_COLOR: 'Цвет',
            car.INDEX_MILEAGE: 'Пробег авто',
        }


    def test_int_fields_2(self):
        self.parkingId = 'Стоянка'
        wrongFields = list(TEST_CARS[0])
        dictPOST = CarDictPOST.create_dict_POST(wrongFields)
        self.then_error_status(dictPOST, 'error_not_int')


    def test_color(self):
        rightFields = list(TEST_CARS[0])
        dictPOST = CreateCarTest.given_wrong_color(rightFields)
        self.then_error_status(dictPOST, 'error_color')


    def given_wrong_color(wrongColorFields: list):
        wrongColorFields[car.INDEX_COLOR] = '100'
        return CarDictPOST.create_dict_POST(wrongColorFields)


    def test_goverment_number(self):
        rightFields = list(TEST_CARS[0])
        givenDictPOSTs = CreateCarTest.given_wrong_gov_number(rightFields)
        self.then_many_error_status(givenDictPOSTs, 'error_gov_number')


    def given_wrong_gov_number(rightFields: list) -> list:
        givenDictPOSTs = list()
        wrongGovNumbers = ('о', 'номеррррр', '123456789', '7ррр77ййй')
        for govNumber in wrongGovNumbers:
            wrongGovNumberFields = rightFields.copy()
            wrongGovNumberFields[car.INDEX_GOV_CAR_NUMBER] = govNumber
            dictPOST = CarDictPOST.create_dict_POST(wrongGovNumberFields)
            givenDictPOSTs.append(dictPOST)
        return givenDictPOSTs


    def test_not_right_phone(self):
        rightFields = list(TEST_CARS[0])
        givenDictPOSTs = CreateCarTest.given_wrong_phone(rightFields)
        self.then_many_error_status(givenDictPOSTs, 'error_phone')


    def given_wrong_phone(rightFields: list) -> list:
        givenDictPOSTs = list()
        wrongPhones = ('123', 'Не_числоооо', '8900123123+')
        for phone in wrongPhones:
            wrongPhoneFields = rightFields.copy()
            wrongPhoneFields[car.INDEX_PHONE] = phone
            dictPOST = CarDictPOST.create_dict_POST(wrongPhoneFields)
            givenDictPOSTs.append(dictPOST)
        return givenDictPOSTs


    def test_parking(self):
        wrongParkingFields = list(TEST_CARS[0])
        self.parkingId += 10
        dictPOST = CarDictPOST.create_dict_POST(wrongParkingFields)
        self.then_error_status(dictPOST, 'error_parking_find')


    def test_model(self):
        wrongModelFields = list(TEST_CARS[0])
        dictPOST = CarDictPOST.create_dict_POST(wrongModelFields)
        self.then_error_status(dictPOST, 'error_model_find')


    def test_add_brands_from_xml(self):
        tcmSupplier = TestCarModelSupplier()
        tcmSupplier.add_car_models_from_file(5)


    def test_add_car(self):
        tcmSupplier = self._given_one_model()
        modelId = tcmSupplier.get_car_model_id()
        dictPOST = CreateCarTest.create_dict_POST_with_model(
            TEST_CARS[0], self.parkingId, modelId)
        self.then_error_status(dictPOST, 'not_errors')


    def test_add_2_cars_in_one_parking(self):
        tcmSupplier = self._given_one_model()
        modelId = tcmSupplier.get_car_model_id()
        givenDictPOSTs = (
            CreateCarTest.create_dict_POST_with_model(
                TEST_CARS[0], self.parkingId, modelId),
            CreateCarTest.create_dict_POST_with_model(
                TEST_CARS[1], self.parkingId, modelId),
        )
        self.then_many_error_status(givenDictPOSTs, 'not_errors')


    def test_add_car_with_new_brand(self):
        dictPOST = CreateCarTest.create_dict_POST_without_model(
            TEST_CARS[0], self.parkingId)

        newModelFields = ['Новая модель', 'Новая марка', TEST_VALUES[0][2]]
        dictPOST.update(ModelDictPOST.create_dict_POST(newModelFields))
        self.then_error_status(dictPOST, 'not_errors')


    def test_add_car_with_new_model(self):
        dictPOST = CreateCarTest.create_dict_POST_without_model(
            TEST_CARS[0], self.parkingId)

        tcmSupplier = self._given_one_model()
        brandId = tcmSupplier.get_car_brand_id()
        newModelFields = ['Новая модель', brandId, TEST_VALUES[1][2]]
        dictPOST.update(ModelDictPOST.create_dict_POST(newModelFields))

        self.then_error_status(dictPOST, 'not_errors')


    def test_add_car_with_model_repeat(self):
        dictPOST = CreateCarTest.create_dict_POST_without_model(
            TEST_CARS[0], self.parkingId)

        tcmSupplier = self._given_one_model()
        modelId = tcmSupplier.get_car_model_id()
        modelData = tcmSupplier.get_car_model_data(modelId)
        newModelFields = [
            modelData[car_model.INDEX_NEW_MODEL],
            modelData[car_model.INDEX_NEW_BRAND],
            TEST_VALUES[1][2],
        ]
        dictPOST.update(ModelDictPOST.create_dict_POST(newModelFields))

        self.then_error_status(dictPOST, 'error_model_already_exists')


    def test_not_unique_goverment_number(self):
        tcSupplier = TestCarSupplier(tpSupplier = self.tpSupplier)
        tcSupplier.add_cars(1)
        carId = tcSupplier.get_car_id()
        repeatGovNumberFields = tcSupplier.get_car_data(carId)
        dictPOST = CarDictPOST.create_dict_POST(repeatGovNumberFields)
        self.then_error_status(dictPOST, 'error_gov_number_used')


    def _given_one_model(self) -> TestCarModelSupplier:
        tcmSupplier = TestCarModelSupplier()
        tcmSupplier.add_car_models(1)
        return tcmSupplier


    def create_dict_POST_with_model(fields: tuple, parkingId: int, modelId: int) -> dict:
        rightFields = list(fields)
        rightFields[car.INDEX_ID_NUMBER] = parkingId
        rightFields[car.INDEX_MODEL] = modelId
        return CarDictPOST.create_dict_POST(rightFields)


    def create_dict_POST_without_model(fields: tuple, parkingId: int) -> dict:
        dictPOST = CreateCarTest.create_dict_POST_with_model(fields, parkingId, -1)
        del dictPOST[car.CarCheckAbstract.CAR_FIELDS[car.INDEX_MODEL]]
        return dictPOST


class UpdateCarTest(ErrorCheckingTestCase):
    TEST_UPDATE = ('о342бн77', 'Стоянка', 'Модель', '5', '12000', 'Артур', 'Петров', 'Григорьевич')


    def setUp(self):
        self.tcSupplier = TestCarSupplier()
        self.tcSupplier.add_cars()
        self.carId = self.tcSupplier.get_car_id(0)


    # override
    def create_testing_object(self, dictPOST: dict):
        return car.CarUpdate(dictPOST, self.carId)


    def test_empty_fields(self):
        givenDictPOSTs = ErrorCheckingTestCase.given_dict_POST_from_empty_fields_check(
            CarDictPOST.create_dict_POST, TEST_CARS[0], car.INDEX_OWNER_SURNAME)
        self.then_many_error_status(givenDictPOSTs, 'error_field')


    def test_int_fields(self):
        givenValues = CreateCarTest.given_wrong_int_fields()
        givenValues[car.INDEX_ID_NUMBER] = 'Стоянка'
        givenDictPOSTs = ErrorCheckingTestCase.given_dict_POST_from_int_fields_check(
            CarDictPOST.create_dict_POST, TEST_CARS[0], givenValues)
        self.then_many_error_status(givenDictPOSTs, 'error_not_int')


    def test_color(self):
        updatingCar = self.tcSupplier.get_car_data(self.carId)
        dictPOST = CreateCarTest.given_wrong_color(updatingCar)
        self.then_error_status(dictPOST, 'error_color')


    def test_goverment_number(self):
        rightFields = self.tcSupplier.get_car_data(self.carId)
        givenDictPOSTs = CreateCarTest.given_wrong_gov_number(rightFields)
        self.then_many_error_status(givenDictPOSTs, 'error_gov_number')


    def test_not_unique_goverment_number(self):
        carId = self.tcSupplier.get_car_id(1)
        repeatGovNumberFields = self.tcSupplier.get_car_data(carId)
        dictPOST = CarDictPOST.create_dict_POST(repeatGovNumberFields)
        self.then_error_status(dictPOST, 'error_gov_number_used')


    def test_not_right_phone(self):
        rightFields = self.tcSupplier.get_car_data(self.carId)
        givenDictPOSTs = CreateCarTest.given_wrong_phone(rightFields)
        self.then_many_error_status(givenDictPOSTs, 'error_phone')


    def test_parking(self):
        wrongParkingFields = self.tcSupplier.get_car_data(self.carId)
        wrongParkingFields[car.INDEX_ID_NUMBER] += 10
        dictPOST = CarDictPOST.create_dict_POST(wrongParkingFields)
        self.then_error_status(dictPOST, 'error_parking_find')


    def test_model(self):
        wrongModelFields = self.tcSupplier.get_car_data(self.carId)
        wrongModelFields[car.INDEX_MODEL] += 100
        dictPOST = CarDictPOST.create_dict_POST(wrongModelFields)
        self.then_error_status(dictPOST, 'error_model_find')


    def test_update_without_changes(self):
        rightFields = self.tcSupplier.get_car_data(self.carId)
        dictPOST = CarDictPOST.create_dict_POST(rightFields)
        self.then_error_status(dictPOST, 'not_errors')


    def test_update_all_fields(self):
        rightFields = list(self.TEST_UPDATE)
        tpSupplier = self.tcSupplier.get_test_parking_supplier()
        parkingId = tpSupplier.get_random_parking_id()
        tcmSupplier = self.tcSupplier.get_test_car_model_supplier()
        modelId = tcmSupplier.get_random_car_model_id()
        dictPOST = CreateCarTest.create_dict_POST_with_model(
            rightFields, parkingId, modelId)
        self.then_error_status(dictPOST, 'not_errors')


    def test_update_with_new_model(self):
        rightFields = self.tcSupplier.get_car_data(self.carId)
        dictPOST = CreateCarTest.create_dict_POST_without_model(
            rightFields, rightFields[car.INDEX_ID_NUMBER])

        tcmSupplier = self.tcSupplier.get_test_car_model_supplier()
        brandId = tcmSupplier.get_car_brand_id()
        newModelFields = ['Новая модель', brandId, TEST_VALUES[1][2]]
        dictPOST.update(ModelDictPOST.create_dict_POST(newModelFields))
        self.then_error_status(dictPOST, 'not_errors')


    def test_update_with_new_model_and_brand(self):
        rightFields = self.tcSupplier.get_car_data(self.carId)
        dictPOST = CreateCarTest.create_dict_POST_without_model(
            rightFields, rightFields[car.INDEX_ID_NUMBER])

        newModelFields = ['Новая модель', 'Новая марка', TEST_VALUES[0][2]]
        dictPOST.update(ModelDictPOST.create_dict_POST(newModelFields))
        self.then_error_status(dictPOST, 'not_errors')


class DeleteCarTest(ErrorCheckingTestCase):
    def setUp(self):
        self.tcSupplier = TestCarSupplier()
        self.tcSupplier.add_cars(2)
        self.deleteCarId = None


    # override
    def create_testing_object(self, dictPOST: dict):
        return car.CarDelete(self.deleteCarId)


    def test_id_not_right(self):
        self.deleteCarId = -10
        self.then_error_status(None, 'error_car_find')
        self.deleteCarId = self.tcSupplier.get_car_id() + 10
        self.then_error_status(None, 'error_car_find')


    def test_delete(self):
        self.deleteCarId = self.tcSupplier.get_car_id()
        self.then_error_status(None, 'not_errors')


class SearchCarTest(ErrorCheckingTestCase):
    def setUp(self):
        self.tcSupplier = TestCarSupplier()
        self.tcSupplier.add_cars()
        self.searchCarId = self.tcSupplier.get_car_id()
        carData = self.tcSupplier.get_car_data(self.searchCarId)
        self.searchGovNumber = carData[car.INDEX_GOV_CAR_NUMBER]


    # override
    def create_testing_object(self, dictPOST: dict):
        return car.SearchCar(dictPOST)


    def test_empty_fields(self):
        givenDictPOSTs = ErrorCheckingTestCase.given_dict_POST_from_empty_fields_check(
            SearchDictGET.create_dict_GET, [self.searchGovNumber], 0)
        self.then_many_error_status(givenDictPOSTs, 'error_field')


    def test_goverment_number(self):
        rightFields = ['Только государственный номер']
        givenDictPOSTs = SearchCarTest.given_wrong_gov_number(rightFields)
        self.then_many_error_status(givenDictPOSTs, 'error_gov_number')


    def given_wrong_gov_number(rightFields: list) -> list:
        givenDictGETs = list()
        wrongGovNumbers = ('Более 9 симолов',)
        for govNumber in wrongGovNumbers:
            wrongGovNumberFields = rightFields.copy()
            wrongGovNumberFields[0] = govNumber
            dictGET = SearchDictGET.create_dict_GET(wrongGovNumberFields)
            givenDictGETs.append(dictGET)
        return givenDictGETs


    def test_search_car(self):
        rightFields = [self.searchGovNumber]
        dictGET = SearchDictGET.create_dict_GET(rightFields)
        self.then_error_status(dictGET, 'not_errors')
        search = self.create_testing_object(dictGET)
        carCount = search.get_count()
        self.assertEqual(1, carCount)


    def test_search_car_not_found(self):
        rightFields = ['н000ет555']
        dictGET = SearchDictGET.create_dict_GET(rightFields)
        self.then_error_status(dictGET, 'not_errors')
        search = self.create_testing_object(dictGET)
        carCount = search.get_count()
        self.assertEqual(0, carCount)
