from django.test import TestCase
from manager.data.errors_check import ErrorsCheckAbstract


class ErrorCheckingTestCase(TestCase):
    def create_testing_object(self, dictPOST: dict) -> ErrorsCheckAbstract:
        pass


    def given_dict_POST_from_empty_fields_check(create_dict_POST_method: 'def', rightFields: tuple, lastEmptyIndex: int) -> list:
        givenDictPOSTs = [
            dict(),
            {'Некорректный': 'словарь POST'},
        ]
        rightFields = list(rightFields)
        for index in range(lastEmptyIndex+1):
            emptyFields = rightFields[:index]
            dictPOST = create_dict_POST_method(emptyFields)
            givenDictPOSTs.append(dictPOST)
        return givenDictPOSTs


    def given_dict_POST_from_int_fields_check(create_dict_POST_method: 'def', rightFields: tuple, wrongIntFields: dict) -> list:
        givenDictPOSTs = list()
        rightFields = list(rightFields)
        for index, value in wrongIntFields.items():
            if isinstance(value, tuple):
                for v in value:
                    dictPOST = ErrorCheckingTestCase._create_dict_POST_from_int_fields(
                        create_dict_POST_method, rightFields, index, v)
                    givenDictPOSTs.append(dictPOST)
            else:
                dictPOST = ErrorCheckingTestCase._create_dict_POST_from_int_fields(
                    create_dict_POST_method, rightFields, index, value)
                givenDictPOSTs.append(dictPOST)
        return givenDictPOSTs


    def _create_dict_POST_from_int_fields(create_dict_POST_method: 'def', rightFields: list, index: int, value):
        wrongFields = rightFields.copy()
        wrongFields[index] = value
        return create_dict_POST_method(wrongFields)


    def then_many_error_status(self, manyDictPOSTs: list, errStatus: str):
        for dictPOST in manyDictPOSTs:
            self.then_error_status(dictPOST, errStatus)


    def then_error_status(self, dictPOST: dict, errStatus: str):
        ''' Проверка статуса ошибки в проверяемом объекте '''
        testingObject = self.create_testing_object(dictPOST)
        whatErrorShouldBe = testingObject.ERR_STATUS[errStatus]
        self.assertEqual(testingObject.get_status(), self._what_status_should_be(whatErrorShouldBe))
        self.assertEqual(testingObject.err_status, whatErrorShouldBe)


    def _what_status_should_be(self, errStatus: str) -> bool:
        return True if errStatus == ErrorsCheckAbstract.ERR_STATUS['not_errors'] else False
