from manager.app_tests.error_checking import ErrorCheckingTestCase
from django.test import TestCase
from manager.app_tests.suppliers.parking_supplier import *
from manager.app_tests.suppliers.employee_supplier import TestEmployeeSupplier
from manager.app_tests.suppliers.car_supplier import TestCarSupplier
from manager.data import parking


class CreateParkingTest(ErrorCheckingTestCase):
    # override
    def create_testing_object(self, dictPOST: dict):
        return parking.ParkingAddNew(dictPOST)


    def test_empty_fields(self):
        givenDictPOSTs = ErrorCheckingTestCase.given_dict_POST_from_empty_fields_check(
            ParkingDictPOST.create_dict_POST, TEST_PARKINGS[0], parking.INDEX_MANAGER)
        self.then_many_error_status(givenDictPOSTs, 'error_field')


    def test_int_fields(self):
        givenValues = CreateParkingTest.given_wrong_int_fields()
        givenDictPOSTs = ErrorCheckingTestCase.given_dict_POST_from_int_fields_check(
            ParkingDictPOST.create_dict_POST, TEST_PARKINGS[0], givenValues)
        self.then_many_error_status(givenDictPOSTs, 'error_not_int')


    def given_wrong_int_fields() -> dict:
        return {
            parking.INDEX_PARKING_NUMBER: ('Номер', '0',),
            parking.INDEX_MANAGER: 'АйДи',
        }


    def test_employee_not_found(self):
        teSupplier = TestEmployeeSupplier()
        teSupplier.add_employees(1)
        dictPOST = CreateParkingTest.employee_not_found(teSupplier)
        self.then_error_status(dictPOST, 'error_employee_find')


    def employee_not_found(teSupplier: TestEmployeeSupplier) -> dict:
        idEmployee = teSupplier.get_employee_id()
        idWrong = str(idEmployee + 30) # сотрдуника с таким id нет
        dictPOST = ParkingDictPOST.create_dict_POST(TEST_PARKINGS[0], idEmployee=idWrong)
        return dictPOST


    def test_employee_not_parking_manager(self):
        teSupplier = TestEmployeeSupplier()
        teSupplier.add_employees(2)
        givenDictPOSTs = CreateParkingTest.employee_not_parking_manager(teSupplier)
        self.then_many_error_status(givenDictPOSTs, 'error_employee_position')


    def employee_not_parking_manager(teSupplier: TestEmployeeSupplier) -> tuple:
        idAdmin = teSupplier.get_admin_id()
        idSecretary = teSupplier.get_secretary_id()
        givenDictPOSTs = (
            ParkingDictPOST.create_dict_POST(TEST_PARKINGS[0], idEmployee=idAdmin),
            ParkingDictPOST.create_dict_POST(TEST_PARKINGS[0], idEmployee=idSecretary),
        )
        return givenDictPOSTs


    def test_add_parking(self):
        teSupplier = TestEmployeeSupplier()
        teSupplier.add_parking_managers(1)
        idParkingManager = teSupplier.get_parking_manager_id()
        dictPOST = ParkingDictPOST.create_dict_POST(TEST_PARKINGS[0], idEmployee=idParkingManager)
        self.then_error_status(dictPOST, 'not_errors')


    def test_not_unique_number(self):
        parkingData = CreateParkingTest._given_one_parking()
        dictPOST = CreateParkingTest.repeat_number(parkingData)
        self.then_error_status(dictPOST, 'error_number_used')


    def test_not_unique_title(self):
        parkingData = CreateParkingTest._given_one_parking()
        dictPOST = CreateParkingTest.repeat_title(parkingData)
        self.then_error_status(dictPOST, 'error_title_used')


    def _given_one_parking():
        tpSupplier = TestParkingSupplier()
        tpSupplier.add_parking_lots(1)
        idParking = tpSupplier.get_parking_id()
        return tpSupplier.get_parking_data(idParking)


    def repeat_number(parkingData: list) -> dict:
        repeatNumberFields = list(TEST_PARKINGS[2])
        repeatNumberFields[parking.INDEX_PARKING_NUMBER] = parkingData[parking.INDEX_PARKING_NUMBER]
        return ParkingDictPOST.create_dict_POST(repeatNumberFields)


    def repeat_title(parkingData: list) -> dict:
        repeatTitleFields = list(TEST_PARKINGS[2])
        repeatTitleFields[parking.INDEX_TITLE] = parkingData[parking.INDEX_TITLE]
        return ParkingDictPOST.create_dict_POST(repeatTitleFields)


    def test_manager_used(self):
        parkingData = CreateParkingTest._given_one_parking()
        idParkingManager = str(parkingData[parking.INDEX_MANAGER])
        dictPOST = ParkingDictPOST.create_dict_POST(TEST_PARKINGS[1], idEmployee=idParkingManager)
        self.then_error_status(dictPOST, 'error_employee_used')


class UpdateParkingTest(ErrorCheckingTestCase):
    def setUp(self):
        self.tpSupplier = TestParkingSupplier()
        self.tpSupplier.add_parking_lots(2)
        self.updatingParkingId = self.tpSupplier.get_parking_id(0)


    # override
    def create_testing_object(self, dictPOST: dict):
        return parking.ParkingUpdate(dictPOST, self.updatingParkingId)


    def test_correct_updating_parking_data(self):
        values = parking.ParkingCheckAbstract.PARKING_VALUES
        employeeValues = self.tpSupplier.get_parking_data(self.updatingParkingId)
        rightDict = {values[i]: employeeValues[i] for i in range(len(employeeValues))}
        testDict = parking.Parking.get_parking_values(self.updatingParkingId)
        self.assertTrue(testDict == rightDict)


    def test_empty_fields(self):
        givenDictPOSTs = ErrorCheckingTestCase.given_dict_POST_from_empty_fields_check(
            ParkingDictPOST.create_dict_POST, TEST_PARKINGS[0], parking.INDEX_MANAGER)
        self.then_many_error_status(givenDictPOSTs, 'error_field')


    def test_int_fields(self):
        givenValues = CreateParkingTest.given_wrong_int_fields()
        givenDictPOSTs = ErrorCheckingTestCase.given_dict_POST_from_int_fields_check(
            ParkingDictPOST.create_dict_POST, TEST_PARKINGS[0], givenValues)
        self.then_many_error_status(givenDictPOSTs, 'error_not_int')


    def test_update_not_unique_number(self):
        idParking = self.tpSupplier.get_parking_id(1)
        parkingData = self.tpSupplier.get_parking_data(idParking)
        dictPOST = CreateParkingTest.repeat_number(parkingData)
        self.then_error_status(dictPOST, 'error_number_used')


    def test_update_not_unique_title(self):
        idParking = self.tpSupplier.get_parking_id(1)
        parkingData = self.tpSupplier.get_parking_data(idParking)
        dictPOST = CreateParkingTest.repeat_title(parkingData)
        self.then_error_status(dictPOST, 'error_title_used')


    def test_employee_not_found(self):
        teSupplier = self.tpSupplier.getTestEmployeeSupplier()
        dictPOST = CreateParkingTest.employee_not_found(teSupplier)
        self.then_error_status(dictPOST, 'error_employee_find')


    def test_employee_not_parking_manager(self):
        teSupplier = self.tpSupplier.getTestEmployeeSupplier()
        teSupplier.add_employees(2) # добавить админа и секретаря
        givenDictPOSTs = CreateParkingTest.employee_not_parking_manager(teSupplier)
        self.then_many_error_status(givenDictPOSTs, 'error_employee_position')


    def test_update_parking_without_update_manager(self):
        parkingData = self.tpSupplier.get_parking_data(self.updatingParkingId)
        parkingManagerId = str(parkingData[parking.INDEX_MANAGER])
        dictPOST = ParkingDictPOST.create_dict_POST(TEST_PARKINGS[2], idEmployee=parkingManagerId)
        self.then_error_status(dictPOST, 'not_errors')


    def test_update_parking_swap_managers(self):
        teSupplier = self.tpSupplier.getTestEmployeeSupplier()
        teSupplier.add_employees(1) # добавить админа
        parkingData = self.tpSupplier.get_parking_data(self.updatingParkingId)
        parking2Id = self.tpSupplier.get_parking_id(1)
        parking2Data = self.tpSupplier.get_parking_data(parking2Id)
        parkingManagerId = str(parking2Data[parking.INDEX_MANAGER])
        dictPOST = ParkingDictPOST.create_dict_POST(parkingData, idEmployee=parkingManagerId)
        self.then_error_status(dictPOST, 'not_errors')


class DeleteParkingTest(ErrorCheckingTestCase):
    def setUp(self):
        self.tpSupplier = TestParkingSupplier()
        self.tpSupplier.add_parking_lots(1)
        self.deleteParkingId = None


    # override
    def create_testing_object(self, dictPOST: dict):
        return parking.ParkingDelete(self.deleteParkingId)


    def test_id_not_right(self):
        self.deleteParkingId = -10
        self.then_error_status(None, 'error_parking_find')
        self.deleteParkingId = self.tpSupplier.get_parking_id() + 10
        self.then_error_status(None, 'error_parking_find')


    def test_delete(self):
        self.deleteParkingId = self.tpSupplier.get_parking_id()
        self.then_error_status(None, 'not_errors')


    def test_delete_parking_with_cars(self):
        tcSupplier = TestCarSupplier(self.tpSupplier)
        tcSupplier.add_cars(2)
        self.deleteParkingId = self.tpSupplier.get_parking_id()
        self.then_error_status(None, 'error_has_cars')
