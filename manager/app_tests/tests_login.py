from django.test import TestCase
from manager.app_tests.suppliers.employee_supplier import *
from manager.data import login, employee
from manager.views import SessionStorageEmployee


class LoginTest(TestCase):
    def test_empty_password(self):
        whenEmptyPassword = ''
        self._then_error_status(whenEmptyPassword, 'error_field')


    def test_create_main_administrator(self):
        whenAdminPassword = TEST_EMPLOYEES[0][employee.INDEX_PASSWORD]
        self._then_error_status(whenAdminPassword, 'not_errors')


    def test_not_right_password_without_emloyees(self):
        whenPassword = 'NtRghtPsswrd'
        self._then_error_status(whenPassword, 'error_password')


    def test_not_right_password_with_emloyees(self):
        teSupplier = TestEmployeeSupplier()
        teSupplier.add_employees()
        whenPassword = 'NtRghtPsswrd'
        self._then_error_status(whenPassword, 'error_password')


    def test_login_administrator(self):
        teSupplier = TestEmployeeSupplier()
        teSupplier.add_employees(1)
        whenAdminPassword = TEST_EMPLOYEES[0][employee.INDEX_PASSWORD]
        self._then_error_status(whenAdminPassword, 'not_errors')


    def test_not_create_main_administrator(self):
        # Администратор добавлен в систему,
        # поэтому автоматически создаваться он не должен
        teSupplier = TestEmployeeSupplier()
        teSupplier.add_employees(1)
        # Изменяем пароль администратора
        adminId = teSupplier.get_admin_id()
        adminData = teSupplier.get_employee_data(adminId)
        adminData[employee.INDEX_PASSWORD] = 'NewPassword'
        dictPOST = EmployeeDictPOST.create_dict_POST(adminData, addPasswords=True)
        employee.EmployeeUpdate(dictPOST, adminId, adminId)
        # Попытка входа с паролем по умолчанию, создающий администратора
        whenAdminPassword = TEST_EMPLOYEES[0][employee.INDEX_PASSWORD]
        self._then_error_status(whenAdminPassword, 'error_password')


    def test_login_employee(self):
        teSupplier = TestEmployeeSupplier()
        teSupplier.add_employees(2)
        whenPassword = TEST_EMPLOYEES[1][employee.INDEX_PASSWORD]
        self._then_error_status(whenPassword, 'not_errors')


    def test_correct_return(self):
        teSupplier = TestEmployeeSupplier()
        teSupplier.add_employees(2)
        rightEmployeeId = teSupplier.get_employee_id(1)
        whenPassword = TEST_EMPLOYEES[1][employee.INDEX_PASSWORD]
        l = login.Login(whenPassword)
        thenEmployeeId = l.get_login_id()
        self.assertEqual(rightEmployeeId, thenEmployeeId)

        thenDict = self._get_employee_login_data(thenEmployeeId)
        rightDict = self._what_dict_should_get(rightEmployeeId)
        self.assertTrue(thenDict == rightDict)


    def _get_employee_login_data(self, employeeId: int) -> dict:
        testSession = dict()
        SessionStorageEmployee.add_employee_data_in_session(testSession, employeeId)
        return testSession['employee']


    def _what_dict_should_get(self, rightEmployeeId) -> dict:
        employeeData = TEST_EMPLOYEES[1][:employee.INDEX_POSITION+1]
        fieldsValues = employee.EmployeeCheckAbstract.EMPLOYEE_VALUES
        rightDict = {
            fieldsValues[i]: employeeData[i]
            for i in range(len(employeeData))
        }
        rightDict['employee_id'] = rightEmployeeId
        return rightDict


    def _then_error_status(self, password: str, errStatus: str):
        ''' Проверка статуса ошибки при добавлении сотрудника '''
        l = login.Login(password)
        self.assertEqual(l.is_login_correct(), _what_status_should_be(errStatus))
        self.assertEqual(l.err_status, l.ERR_STATUS[errStatus])


def _what_status_should_be(errStatus: str) -> bool:
    return True if errStatus == 'not_errors' else False
