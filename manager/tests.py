from django.test import TestCase
from manager.app_tests import (
    tests_employee,
    tests_login,
    tests_restrictions,
    tests_parking,
    tests_car,
    tests_documents,
)


'''
Чтобы запустить тесты с предыдущей БД, нужно вызывать тесты как:
py manage.py test manager --keepdb
'''


class CreateEmployeeTest(tests_employee.CreateEmployeeTest):
    pass


class UpdateEmployeeTest(tests_employee.UpdateEmployeeTest):
    pass


class EmployeeListTest(tests_employee.EmployeeListTest):
    pass


class DeleteEmployeesTest(tests_employee.DeleteEmployeesTest):
    pass


class ManagersListTest(tests_employee.ManagersListTest):
    pass


class LoginTest(tests_login.LoginTest):
    pass


class CreateParkingTest(tests_parking.CreateParkingTest):
    pass


class UpdateParkingTest(tests_parking.UpdateParkingTest):
    pass


class DeleteParkingTest(tests_parking.DeleteParkingTest):
    pass


class CreateModelTest(tests_car.CreateModelTest):
    pass


class CreateCarTest(tests_car.CreateCarTest):
    pass


class UpdateCarTest(tests_car.UpdateCarTest):
    pass


class DeleteCarTest(tests_car.DeleteCarTest):
    pass


class SearchCarTest(tests_car.SearchCarTest):
    pass


class CreateDocumentsTest(tests_documents.CreateDocumentsTest):
    pass


class PageRestrictionsTest(tests_restrictions.PageRestrictionsTest):
    pass


class FirstLoginTest(tests_restrictions.FirstLoginTest):
    pass
