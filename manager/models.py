from django.db import models


class Employee(models.Model):
    positionChoices = (
        ('a', 'Администратор',),
        ('s', 'Секретарь',),
        ('p', 'Управляющий стоянкой',),
    )

    id_employee = models.SmallAutoField(primary_key=True)
    surname = models.CharField(max_length=20)
    name = models.CharField(max_length=20)
    patronymic = models.CharField(max_length=20, blank=True, default='')
    position = models.CharField(max_length=1, choices=positionChoices)
    phone = models.CharField(max_length=11)
    password = models.CharField(unique=True, max_length=12)


    class Meta:
        db_table = 'employee'


class CarParking(models.Model):
    id_number = models.SmallAutoField(primary_key=True)
    id_employee = models.OneToOneField('Employee', models.RESTRICT, db_column='id_employee')
    parking_number = models.PositiveSmallIntegerField(unique=True)
    title = models.CharField(unique=True, max_length=20)
    district = models.CharField(max_length=20)
    street = models.CharField(max_length=20)

    class Meta:
        db_table = 'car_parking'


class CarBrand(models.Model):
    id_brand = models.SmallAutoField(primary_key=True)
    brand = models.CharField(max_length=20)

    class Meta:
        db_table = 'car_brand'


class CarModel(models.Model):
    id_model = models.SmallAutoField(primary_key=True)
    id_brand = models.ForeignKey('CarBrand', models.CASCADE, db_column='id_brand')
    model = models.CharField(max_length=20)

    class Meta:
        db_table = 'car_model'


class Car(models.Model):
    id_car_number = models.AutoField(primary_key=True)
    gov_car_number = models.CharField(unique=True, max_length=9)
    id_number = models.ForeignKey('CarParking', models.RESTRICT, db_column='id_number')
    id_model = models.ForeignKey('CarModel', models.RESTRICT, db_column='id_model')
    color = models.PositiveSmallIntegerField()
    mileage = models.PositiveIntegerField()
    owner_name = models.CharField(max_length=20)
    owner_surname = models.CharField(max_length=20)
    owner_patronymic = models.CharField(max_length=20, blank=True, default='')
    phone = models.CharField(max_length=11)

    class Meta:
        db_table = 'car'
