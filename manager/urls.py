from django.urls import path
from manager import views

app_name = 'manager'

urlpatterns = [
    path('document_parking_list/', views.document_parking_list, name='document_parking_list'),
    path('document_car_list=<int:id_parking>/', views.document_car_list, name='document_car_list'),
    path('document_all_car_list/', views.document_all_car_list, name='document_all_car_list'),
]
